<?php namespace Controllers;

use Models\UserProfile;
use Models\Users;
use Models\Publish;

use Presenters\PublishPresenter;

class PublishController extends \Controllers\ControllerBase
{
    public function initialize()
    {
        parent::initialize();
    }
    
    public function getAction($id)
    {
        $presenter = new PublishPresenter($this->config);
        return $presenter->getPublish($id);
    }

    public function getAllAction()
    {
        $presenter = new PublishPresenter($this->config);
        return $presenter->getAllPublishList();
    }

    public function getBySearchAction()
    {
        $presenter = new PublishPresenter($this->config);

        $params = [];
        $params["price"] = $this->request->getQuery("price");
        $params["what"] = $this->request->getQuery("what");
        $params["chambres"] = $this->request->getQuery("chambres");
        $params["pieces"] = $this->request->getQuery("pieces");
        $params["location"] = $this->request->getQuery("location");
        $params["habitation"] = $this->request->getQuery("habitation");
        $params["surface"] = $this->request->getQuery("surface");
        $params["loc"] = $this->request->getQuery("loc");

        return $presenter->getBySearch($params);
    }

    public function getAvailableAction()
    {
        $model = new Publish();
        $items = $model->getAvailable();
        if (!$items) return $this->notFound();

        $items = $model->modelFormatOnList($items);
        return $this->successResult($items);
    }

    public function getUserAction()
    {
        $presenter = new PublishPresenter($this->config);
        return $presenter->getUserPublish($this->uid);
    }

    public function newAction()
    {
        $presenter = new PublishPresenter($this->config);
        $input = $this->request->getPost();
        return $presenter->newUserPublish($this->uid, $input);
    }

    public function updateAction()
    {
        $presenter = new PublishPresenter($this->config);
        $input = $this->request->getPost();
        return $presenter->updateUserPublish($this->uid, $input);
    }

    public function updateStatusAction()
    {
        $presenter = new PublishPresenter($this->config);
        $input = $this->request->getPost();
        return $presenter->updatePublishStatus($input);
    }
    
    public function activateAction()
    {
        $presenter = new PublishPresenter($this->config);
        $input = $this->request->getPost();
        return $presenter->activatePublish($input);
    }

    public function removeAction()
    {
        $presenter = new PublishPresenter($this->config);
        $input = $this->request->getPost();
        return $presenter->removeUserPublish($this->uid, $input["id"]);
    }

    public function deleteAction($id)
    {
        $presenter = new PublishPresenter($this->config);
        $input = $this->request->getPost();
        return $presenter->deletePublish($id);
    }

    private function verifyResult($result) 
    {
        if ($result || is_array($result)) 
            return $this->successResult($result);
        else 
            return $this->errorDBResult();
    }
    
    private function errorOnParams()
    {
        $result = array( );
        $result['status'] = 3;
        $result['message'] = "Error found on the parameters!";
        return $result;
    }

    private function podcastNotAvailable()
    {
        $result = array( );
        $result['status'] = 5;
        $result['message'] = "Podcast Not Available";
        return $result;
    }

    private function noCredits()
    {
        $result = array( );
        $result['status'] = 4;
        $result['message'] = "Not enough credits";
        return $result;
    }

    private function notFound()
    {
        $result = array( );
        $result['status'] = 2;
        $result['result'] = [];
        $result['message'] = "Not Found!";
        return $result;
    }

    private function errorDBResult( )
    {
        $result = array( );
        $result['status'] = 4;
        $result['message'] = "Database error!";
        return $result;
    }

    private function successResult($res)
    {
        $result = array( );
        $result['status'] = 1;
        $result['message'] = "Request Success!";
        $result['result'] = $res;
        return $result;
    }

}
