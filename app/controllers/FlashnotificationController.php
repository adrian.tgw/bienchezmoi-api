<?php namespace Controllers;

ini_set('default_socket_timeout', 3600);

use Models\FlashNotification;
use Models\Podcast;
use Models\Users;
use Models\FlashMessage;
use Services\MPushNotifications;
use Services\PaymentService;
use Services\SaveImageService;
use Presenters\UsersPresenter;

class FlashnotificationController extends \Controllers\ControllerBase
{
    public function getAllFlashNotificationsAction()
    {
        $self = new FlashNotification();
        $items = $self->getAllFlashNotifications();
        $items = $self->modelFormatOnList($items);
        return $this->successResult( $items );
    }

    public function getAvailableFlashNotificationsAction()
    {
        $self = new FlashNotification();
        $items = $self->getAvailableFlashNotifications();
        $items = $self->modelFormatOnList($items);
        return $this->successResult( $items );
    }

    public function getSentNotificationsAction()
    {
        $self = new FlashNotification();
        $items = $self->getSentFlashNotifications( );
        $items = $self->modelFormatOnList($items);
        return $this->successResult( $items );
    }

    public function getFlashNotificationAction($id)
    {
        $self = new FlashNotification();
        return $this->verifyItemFound( $self->modelFormat($self->getFlashNotificationById($id)) );
    }

    public function sendPushNotificationAction($id)
    {
        $self = new FlashNotification();
        $flashNoti = $self->getFlashNotificationById($id);

        if ( !$flashNoti ) 
        {
            return $this->verifyResult( false );
        }

        //$result = $this->searchDeviceUsersWithTopic("flashes");
        $input = $this->request->getPost();
        $result = $this->searchDeviceUsersWithTopic("flashes");
        if ($result['status'] != 1 || count($result['result']) <= 0) {
            return ['status' => 4, 'message' => 'There are no devices found with the selected topic'];
        }

        $flashMessage = new FlashMessage(
            $flashNoti->title, 
            $flashNoti->short,
            (string) $flashNoti->_id
        );

        $userDevices = $result['result'];

        $newList = [];
        for ($ndex = 0; $ndex < 1; $ndex++) {
            foreach($userDevices as $user) {
                $newList[] = $user;
            }
        }

        $pushNotifications = new MPushNotifications();
        $response = $pushNotifications->sendMultipleNotifications($newList, $flashMessage);

        $flashNoti->updateFlashData($response['results']);
        
        return $this->verifyResult( $flashNoti->update() );
    }

    public function newNotificationAction()
    {
        $input = $this->request->getPost();
        $flashNotification = new FlashNotification();
        $flashNotification = $flashNotification->newFlashNotification( $input );
        $itemSaved = $flashNotification->register();

        $saveImage = new SaveImageService();
        $itemId = (string) $itemSaved->getId();
        $imagesNames = $saveImage->saveFlashPhotos($input, $itemId);
        
        $itemSaved->setImages( $imagesNames );
        $itemSaved = $itemSaved->update();
        
        return $this->verifyResult( $itemSaved );   
    }

    public function updateNotificationAction($id)
    {
        $self = new FlashNotification();
        $flashNotification = $self->getFlashNotificationById($id);

        if ( !$flashNotification ) 
        {
            return $this->verifyResult( false );
        }

        $input = $this->request->getPost();
        $flashNotification = $flashNotification->updateFlashNotification( $input );
        $itemSaved = $flashNotification->update();

        $saveImage = new SaveImageService();
        $itemId = (string) $itemSaved->getId();
        $imagesNames = $saveImage->saveFlashPhotos($input, $itemId);
        
        $itemSaved->setImages( $imagesNames );
        $itemSaved = $itemSaved->update();
        
        return $this->verifyResult( $itemSaved );   
    }

    public function deleteNotificationAction($id)
    {
        $model = new FlashNotification();
        $item = $model->getFlashNotificationById($id);

        if ( $item ) {
            return $this->verifyResult( $item->deleteNotification() );
        }
        
        return $this->verifyResult( false );
    }

    public function searchDeviceUsersWithProfile($input, $topic)
    {
        $profile = isset($input["profile"]) ? intval($input["profile"]) : 0;
        $self = false;

        if ($profile === 0) {
            $self = Users::find(
                [
                    [
                        "notification" => true,
                        '$or' => [
                            ['topics' => ['$all' => [$topic]] ]
                        ],  
                    ]
                ]
            );
        }
        //echo json_encode($self, JSON_PRETTY_PRINT);

        if ( $self === false )
            return ['status' => 2, 'message' => 'nothing found based on your query: '];
        else
            return ['status' => 1, 'result' => $self];
    }

    public function searchDeviceUsersWithTopic($topic)
    {
        $self = Users::find(
            [
                [
                    "notification" => true,
                    '$or' => [
                        ['topics' => ['$all' => [$topic]] ]
                    ],  
                ]
            ]
        );

        if ( $self === false )
            return ['status' => 2, 'message' => 'nothing found based on your query: '];
        else
            return ['status' => 1, 'result' => $self];
    }
    

    private function verifyItemFound($result)
    {
        if ($result) 
            return $this->successResult($result);
        else if (!is_array($result))
            return $this->notFound();
        else 
            return $this->errorDBResult();
    }

    private function verifyResult($result) 
    {
        if ($result || is_array($result)) 
            return $this->successResult($result);
        else 
            return $this->errorDBResult();
    }

    private function errorOnParams()
    {
        $result = array( );
        $result['status'] = 3;
        $result['message'] = "Error found on the parameters!";
        return $result;
    }

    private function notFound()
    {
        $result = array( );
        $result['status'] = 2;
        $result['result'] = [];
        $result['message'] = "Not Found!";
        return $result;
    }

    private function errorDBResult( )
    {
        $result = array( );
        $result['status'] = 4;
        $result['message'] = "Database error!";
        return $result;
    }

    private function successResult( $res )
    {
        $result = array( );
        $result['status'] = 1;
        $result['message'] = "Request Success!";

        $result['result'] = $res;

        return $result;
    }

}
