<?php
declare(strict_types=1);
namespace Controllers;

use Phalcon\Mvc\Dispatcher;
use Models\Location;

class LocationController extends ControllerBase
{
    public function initialize()
    {
        parent::initialize();
    }

    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        $allowedActionsWithoutToken = [
            "getAll"
        ];
        
        if (in_array($dispatcher->getActionName(), $allowedActionsWithoutToken)) 
        {
            $this->isProtectedByToken = false;
        }

        parent::beforeExecuteRoute($dispatcher);

    }

    public function readFromFileAction($fileName) 
    {
        $model = new Location();
        // read from file
        $fp = json_decode(file_get_contents(__DIR__.'/../../storage/locations/'.$fileName.'.json'));
        //echo json_encode($fp, JSON_PRETTY_PRINT);
        $dict = ["pays" => [], "villes" => [], "communes" => [], "quartiers" => []];
        $list = ["pays" => array(), "villes" => array(), "communes" => array(), "quartiers" => array()];

        foreach($fp as $item) {
            //echo json_encode($item, JSON_PRETTY_PRINT);
            if ( !isset($dict["pays"][$item->pays]) ) {
                $dict["pays"][$item->pays] = 1;
                $list["pays"][] = [
                    "value" => $item->pays,
                    "parent" => ""
                ];
            }

            if ( !isset($dict["villes"][$item->villes]) ) {
                $dict["villes"][$item->villes] = 1;
                $list["villes"][] = [
                    "value" => $item->villes,
                    "parent" => $item->pays
                ];
            }

            if ( !isset($dict["communes"][$item->communes]) ) {
                $dict["communes"][$item->communes] = 1;
                $list["communes"][] = [
                    "value" => $item->communes,
                    "parent" => $item->villes
                ];
            }

            if ( !isset($dict["quartiers"][$item->quartiers]) ) {
                $dict["quartiers"][$item->quartiers] = 1;
                $list["quartiers"][] = [
                    "value" => $item->quartiers,
                    "parent" => $item->communes
                ];
            }
        }

        foreach($list["pays"] as $item) {
            if ( !$model->newItemByType(1, 0, $item) )
            {
                die();
            }
        }

        foreach($list["villes"] as $item) {
            if ( !$model->newItemByType(2, 1, $item) )
            {
                die();
            }
        }

        foreach($list["communes"] as $item) {
            if ( !$model->newItemByType(3, 2, $item) )
            {
                die();
            }
        }

        foreach($list["quartiers"] as $item) {
            if ( !$model->newItemByType(4, 3, $item) )
            {
                die();
            }
        }

        //echo json_encode($list, JSON_PRETTY_PRINT);
        return $this->verifyResult(true);
    }


    public function getAction($id)
    {
        $model = new Location();
        return $this->verifyResult($model->getById($id));   
    }

    public function getAllAction()
    {
        return $this->verifyResult([]);   
    }

    public function getAvailableAction()
    {
        $model = new Location();

        $results = $model->getAvailable();

        return $this->verifyResult( $results );   
    }

    public function getByTypeAction($type)
    {
        $model = new Location();

        $results = $model->getAllByType($type);
        return $this->verifyResult($results);
    }

    public function getAvailableByTypeAction($type)
    {
        $model = new Location();

        $results = $model->getAvailableByType($type);
        return $this->verifyResult($results);
    }

    public function getByParentAction($id)
    {
        $model = new Location();

        $results = $model->getAllByParent($id);
        return $this->verifyResult($results);
    }

    public function getAvailableByParentAction($id)
    {
        $model = new Location();

        $results = $model->getAvailableByParent($id);
        return $this->verifyResult($results);
    }

    public function newAction()
    {
        $input = $this->request->getPost();

        $model = new Location();
        $result = $model->registerNew($input);

        return $this->verifyResult( $result );
    }

    public function updateAction($id)
    {
        $input = $this->request->getPost();

        $self = Location::findById($id);
        $result = $self->update($input);

        return $this->verifyResult( $result );
    }

    public function deleteAction($id)
    {
        try
        {
            $model = new Location();
            $item = $model->getByIdObj($id);
            $item->delete();

            return ["status" => 1, "message" => "removed success !" ];   

        } catch ( \Exception $e ) {
            return ["status" => 0, "message" => "Server error !"];
        }
    }

    private function verifyResult($result) 
    {
        if ($result || is_array($result)) 
            return $this->successResult($result);
        else 
            return $this->errorDBResult();
    }

    private function errorDBResult( )
    {
        $result = array( );
        $result['status'] = 4;
        $result['message'] = "Database error!";
        return $result;
    }

    private function successResult($res)
    {
        $result = array( );
        $result['status'] = 1;
        $result['message'] = "Request Success!";
        $result['result'] = $res;
        return $result;
    }

}
