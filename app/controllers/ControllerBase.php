<?php
declare(strict_types=1);
namespace Controllers;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\Controller;
use Phalcon\Http\Request;
use Phalcon\Mvc\Url;

use ParagonIE\Halite\KeyFactory;
use Firebase\JWT\JWT;

class ControllerBase extends Controller
{
    public $ipAddress, 
    $contentType, 
    $request, 
    $url, 
    $config, 
    $now, 
    $uid;

    protected 
    $isProtectedByToken = true, 
    $secretKey, 
    $tokenKey = 'mel3jlHS@w$IJ3Fatr!j_se';

    public function initialize()
    {
        $this->config = $this->di->get('config');
        $this->url = new Url();

        // Create new random encryption key (if doesn't exists) to use as hashing passwords and messages
        if (!\file_exists(BASE_PATH . '/storage/keys/encryption.key')) {
            $this->secretKey = KeyFactory::generateEncryptionKey();
            KeyFactory::save($this->secretKey, BASE_PATH . '/storage/keys/encryption.key');
        } else {
            $this->secretKey = KeyFactory::loadEncryptionKey(BASE_PATH . '/storage/keys/encryption.key');
        }

        // Requests and useful variables
        $this->request = new Request();

        // Get the Http-X-Requested-With header
        $this->requestToken = $this->request->getHeader('token');

        //$this->now = new \MongoDB\BSON\UTCDateTime(time() * 1000);

        if ( $this->tokenExists( $this->requestToken ) ) {
            $this->uid = $this->uid();
        } 
        else 
        {
            $this->uid = false;
        }
        //$requestedWith = $this->request->getHeader("HTTP_X_REQUESTED_WITH");

        // Same as above
        //if ($request->isAjax()) { echo "The request was made with Ajax"; }

        // Check the request layer
        //if ($request->isSecure()) { echo "The request was made using a secure layer"; }

        // Get the client's IP address ie. 201.245.53.51
        $this->ipAddress = $this->request->getClientAddress();

        // Get the best acceptable content by the browser. ie text/xml
        $this->contentType = $this->request->getAcceptableContent();
    }

    public function uid()
    {
        $tokenData = $this->decodeToken($this->requestToken);
        if ( $tokenData !== false ) {
            return $tokenData->uid;
        }

        return false;
    }

    public function decodeToken($jwt)
    {
        $token = is_string($jwt) ? $jwt : $jwt->token;

        try {
            return JWT::decode($token, $this->tokenKey, array('HS256'));   
        }
        catch (Firebase\JWT\ExpiredException $e) {
            
            if ( is_string($token) && strlen($token) > 0 ) {
                $jwt = new \Models\UserToken();
                $jwt->exists(['token' => $token], 'token');

                if ( @$jwt->token )
                    $jwt->delete(); // apaga token antigo
            }

            return false;
        }
        catch (Firebase\JWT\BeforeValidException $e)
        {
            return false;
        }
        catch (\UnexpectedValueException $e)
        {
            return false;
        }
        catch (\Exception $e) 
        {
            return false;
        }
    }

    protected function tokenExists($token)
    {
        try {
            return JWT::decode($token, $this->tokenKey, array('HS256'));

        } catch (\Exception $e) {
            return false;
        }
    }

    protected function tokenIsValid($token, $checker)
    {
        $decodedToken = $this->decodeToken($token);

        return (bool) @$decodedToken && $decodedToken->uid==$checker;
    }

    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        $request = new Request();
        $data = false;
        
        $this->requestToken = $request->getHeader('token');
        if ( $this->isProtectedByToken ) 
        {
            if ( empty($this->requestToken) || !$this->requestToken || $this->requestToken === '' ) 
            {
                $data = [
                    'message' => 'Unauthorized',
                    'code' => 1,
                    'status' => 401
                ];
            }
            else 
            {
                $statusToken = $this->decodeToken($this->requestToken);
                if (!$statusToken) {
                    $data = [
                        'message' => 'Token not valid !',
                        'code' => 2,
                        'status' => 401,
                    ];
                }
                /*
                else {
                    $data = [
                        'message' => 'Token has expired !',
                        'code' => 3,
                        'status' => 401,
                    ];
                }
                */
            }

            if ( $data !== false ) {

                $dispatcher->forward(
                    [
                        'controller' => 'index',
                        'action'     => 'route401',
                        'params' => [
                            'data' => $data
                        ]
                    ]
                );
            }

        }
        
    }

    // After route executed event
    public function afterExecuteRoute(Dispatcher $dispatcher) 
    {
        $this->view->disable();
        $data = $dispatcher->getReturnedValue();

        $response = new \Phalcon\Http\Response();
        $response->setStatusCode(200, "OK");
        //$response->setHeader("Access-Control-Allow-Origin", "http://appbridor.com");
        $response->setHeader("Access-Control-Allow-Origin", "*");

        if ( is_array($data) || is_object($data) ) {
            $response->setContentType("application/json", "UTF-8");
            $json = json_encode($data, JSON_UNESCAPED_SLASHES);
            $response->setContent($json);
        } else {
            $response->setContent($data);
        }

        $response->send();
        exit;
    }

    public function closeSessionAction()
    {
        $this->view->disable();
    }
}
