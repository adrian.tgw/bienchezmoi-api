<?php 
declare(strict_types=1);
namespace Controllers;

require_once __DIR__.'/../../griffinledingham/php-apple-signin/Vendor/JWK.php';
require_once __DIR__.'/../../griffinledingham/php-apple-signin/Vendor/JWT.php';
require_once __DIR__.'/../../griffinledingham/php-apple-signin/ASDecoder.php';

use Phalcon\Mvc\Dispatcher;

use ParagonIE\Halite\HiddenString;
use ParagonIE\Halite\Password;

use Firebase\JWT\JWT;

use Models\Users;
use Token\AuthToken;

use AppleSignIn\ASDecoder;

use Presenters\UsersPresenter;

class AuthController extends ControllerBase
{
    public function initialize()
    {
        parent::initialize();
    }

    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        
        $allowedActionsWithoutToken = [
            "login",
            "loginSocialApple",
            "anonymous"
        ];
        
        if (in_array($dispatcher->getActionName(), $allowedActionsWithoutToken)) 
        {
            $this->isProtectedByToken = false;
        }

        parent::beforeExecuteRoute($dispatcher);

    }

    public function loginAction( )
    {
        $email = $this->request->getPost('email');
        $password = $this->request->getPost('password');

        $input = $this->request->getPost();

        $this->response->setHeader("Access-Control-Allow-Origin", "*");

        $user = new Users();
        if ( $user->exists($input) ) {

            $userFound = $user->getLocalUser($email); 

            if (!$userFound->status) {
                return ['status' => 4, 'message' => 'Login Failed! User is disabled!'];
            }
            
            if ($userFound->status == 2) {
                return ['status' => 3, 'message' => 'Login Failed! You need to confirm your email!'];
            }

            $password = new HiddenString($password);
            if (Password::verify($password, $userFound->getPassword(), $this->secretKey)) {
                $authToken = new AuthToken();
                return [
                    'status' => 1, 
                    'message' => 'Login Success!', 
                    'token' => $authToken->newToken($userFound->_id),
                    'result' => $user->modelFormatMajor( $userFound )
                ];
            }
        }

        return ['status' => 2, 'message' => 'Login Failed! Unregistered Account!'];
    }

    public function loginSocialAppleAction( )
    {
        $identityToken = $this->request->getPost('identityToken');

        $appleSignInPayload = ASDecoder::getAppleSignInPayload($identityToken);

        $email = $appleSignInPayload->getEmail();
        $user = $appleSignInPayload->getUser();

        $userModel = new Users();
        if ( $userModel->existsSocialUser($user) )
        {
            $userFound = $userModel->getLocalUserSocial($user); 

            if (!$userFound->status) {
                return ['status' => 4, 'message' => 'Login Failed! User is disabled!'];
            }

            $authToken = new AuthToken();
            return [
                'status' => 1, 
                'message' => 'Login Success!', 
                'token' => $authToken->newToken($userFound->_id),
                'result' => $userModel->modelFormatMajor( $userFound )
            ];
        } 
        else
        {
            $name = $this->request->getPost('name');
            $lastName = $this->request->getPost('lastName');
            $usersPresenter = new UsersPresenter($this->config);
            $newUser = $usersPresenter->newUserApple($email, $user, $name, $lastName); 

            $authToken = new AuthToken();
            return [
                'status' => 1, 
                'message' => 'Login Success!', 
                'token' => $authToken->newToken($newUser->_id),
                'result' => $userModel->modelFormatMajor( $newUser )
            ];
        }

        return ['status' => 2, 'message' => 'Login Failed! Unregistered Account!'];
    }

    public function anonymousAction( )
    {
        $email = $this->request->getPost('email');
        $password = $this->request->getPost('password');

        //$input = $this->request->getPost();

        $this->response->setHeader("Access-Control-Allow-Origin", "*");

        $user = new Users();
        if ( $email === "anonym@appbienchezmoi.com" ) {

            //$userFound = $user->getLocalUser($email); 

            /*
            if (!$userFound->status) {
                return ['status' => 4, 'message' => 'Login Failed! User is disabled!'];
            }
            
            if ($userFound->status == 2) {
                return ['status' => 3, 'message' => 'Login Failed! You need to confirm your email!'];
            }
            */
            $passwd = new HiddenString($password);
            
            $passwdVerification = "h!!denPa22woRd";
            $passwordVerification = new HiddenString($passwdVerification);
            $hash = Password::hash($passwordVerification, $this->secretKey);
            
            if (Password::verify($passwd, $hash, $this->secretKey)) {
                
                $authToken = new AuthToken();
                return [
                    'status' => 6,
                    'message' => 'Login Success!', 
                    'token' => $authToken->newTokenAnonymous(""),
                    'anonymous' => true
                ];
            }
        }

        return ['status' => 2, 'message' => 'Login Failed! Unregistered Account!'];
    }

    public function statusTokenAction()
    {
        $authToken = new AuthToken();
        $token = $authToken->exists( $this->uid );
        
        if ( (bool) $token && $this->tokenIsValid($token, $this->uid) ) {
            try {
                $userFound = Users::findById($this->uid);
            } catch ( \Exception $e ) {
                var_dump($e->getMessage());
                return;
            }
            
            return ['status' => 'OK', 'token' => $token->token, 'result' => $userFound->modelFormatMajor( $userFound )];

        } else {
            return ['status' => 'ERROR', 'msg' => 'Expired', 'expires_at' => date('Y-m-d H:i:s', $token->expires_at)];
        }
    }

}

