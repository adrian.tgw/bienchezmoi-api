<?php namespace Controllers;

use Phalcon\Mvc\Dispatcher;

use Phalcon\Config;
use Models\Users;
use Models\SNS;
use Models\UserToken;
use Models\ExpoPushNotification;

use Aws\Sns\SnsClient;
use Aws\Exception\AwsException;
use Aws\S3\Exception\SNSException;

use Services\MRequests;
use Services\MPushNotifications;

use Models\FlashMessage;
use Models\Flash;

class SnsController extends ControllerBase
{
    private $expoEndPoint = 'https://exp.host/--/api/v2/push/send';

    public function initialize()
    {
        parent::initialize();
    }

    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        $allowedActionsWithoutToken = [
            "removeUserDevice",
            "updateUserPushNotifications"
        ];
        
        if (in_array($dispatcher->getActionName(), $allowedActionsWithoutToken)) 
        {
            $this->isProtectedByToken = false;
        }

        parent::beforeExecuteRoute($dispatcher);

    }

    public function indexAction()
    {
        return ['status'=> 1, 'result' => true];
    }

    public function applicationsAction()
    {
        $sns = SnsClient::factory(array(
            'credentials' => [
                'key'     => $this->config->aws->key,
                'secret'  => $this->config->aws->secret,
            ],
            'region'  => $this->config->aws->region,
            'version' => $this->config->aws->version
        ));

        $results = $sns->listPlatformApplications();

        if ( !$results )
            return ['status' => 4, 'message' => 'there are not topics!'];

        $items = [];
        foreach ( $results['PlatformApplications'] as $topic ) {
            $items[] = $topic['PlatformApplicationArn'];
        }

        return ['status' => 1, 'result' => $items];
    }

    public function isDeviceRegisteredAction($deviceID)
    {
        $utoken = new UserToken();

        $token = $utoken->exists(['uid' => $this->uid]);

        if ( !(bool) $token && $this->tokenIsValid($token, (string) $this->uid) )
            return $token->token;
        
        try {
            $user = Users::findById($this->uid);
        } catch ( \Exception $e ) {
            var_dump($e->getMessage());
            return;
        }

        if ( !@$user->devices || !count($user->devices) ) {
            return ['status' => 2, 'message' => 'The device has not been registered', 'data' => ['registered' => false]];
        }

        $isDeviceRegistered = false;
        $userDevices = $user->devices;

        foreach($userDevices as $device)
        {
            $token = $device['token'];
            if ($token == $deviceID)
            {
                $isDeviceRegistered = true;
                break;
            }
        }

        if ($isDeviceRegistered) {
            return ['status' => 1, 'message' => 'Device was registered with success!', 'data' => ['registered' => true]];
        }

        return ['status' => 2, 'message' => 'The device has not been registered', 'data' => ['registered' => false]];

    }

    public function userPushNotificationsAction( )
    {
        $utoken = new UserToken();

        $token = $utoken->exists(['uid' => $this->uid]);

        if ( !(bool) $token && $this->tokenIsValid($token, (string) $this->uid) )
            return $token->token;
        
        try {
            $user = Users::findById($this->uid);
        } catch ( \Exception $e ) {
            var_dump($e->getMessage());
            return;
        }

        if ( !@$user->push_messages || !count($user->push_messages) ) {
            return ['status' => 1, 'message' => 'The user does not have push messages', 'data' => ['pushmessages' => [] ]];
        }

        $push_messages = $user->push_messages;

        $results = [];
        foreach($push_messages as $message)
        {
            if ($message['seen'] == false)
            {
                $results[] = $message;
            }
        }

        if (isset($user->badges) && count($push_messages) !== $user->badges)
        {
            $userUpdated = $this->updateUserBadgeAndMessages( $user, count($push_messages), $push_messages );
            try {
                $userUpdated->save();
            } catch(Exception $e) {
                
            }
        }

        return ['status' => 1, 'message' => 'User Push Notifications with success!', 'data' => ['pushmessages' => $results]];
    }

    public function userPushDevicesAction($topic)
    {
        $utoken = new UserToken();
        $token = $utoken->exists(['uid' => $this->uid]);

        if ( !(bool) $token && $this->tokenIsValid($token, (string) $this->uid) )
            return $token->token;
        
        try {
            $user = Users::findById($this->uid);
        } catch ( \Exception $e ) {
            var_dump($e->getMessage());
            return;
        }

        // $result = $this->searchDeviceUsersWithTopic('flashes');
        $result = $this->searchDeviceUsersWithTopic($topic);

        $results = [];
        foreach($result['result'] as $user)
        {
            $username = '';
            if (isset($user->username)) {
                $username = $user->username;
            }

            $push_messages = [];
            if (isset($user->push_messages)) {
                $push_messages = $user->push_messages;
            }

            $pushmessages = [];
            if (isset($user->pushmessages)) {
                $pushmessages = $user->pushmessages;
            }

            $badges = 0;
            if (isset($user->badges)) {
                $badges = $user->badges;
            }

            $email = '';
            if (isset($user->email)) {
                $email = $user->email;
            }

            $results[] = [
                'uid' => $user->getID().'',
                'username' => $username,
                'devices' => $user->devices,
                'push_messages' => $push_messages,
                'pushmessages' => $pushmessages,
                'badges' => $badges,
                'email' => $email,
            ];
        }

        return ['status' => 1, 'message' => 'User Push Notifications with success!', 'data' => ['total' => count($results), 'userdevices' => $results]];
    }

    public function multipleDevicesAction( )
    {
        $utoken = new UserToken();

        $token = $utoken->exists(['uid' => $this->uid]);

        if ( !(bool) $token && $this->tokenIsValid($token, (string) $this->uid) )
            return $token->token;
        
        try {
            $user = Users::findById($this->uid);
        } catch ( \Exception $e ) {
            var_dump($e->getMessage());
            return;
        }

        $result = $this->searchMultipleDevices( );

        // $map = ['uno' => ['count' => 1, 'users' => ['fdfads']]];
        $map = [ ];

        $results = [];
        foreach($result['result'] as $user)
        {
            if (isset($user->username)) {
                $username = $user->username;
            }

            // $pushmessages = [];
            $tokenDevice = $user->devices[0]['token'];
            if (isset($map[$tokenDevice])) {
                $map[$tokenDevice]['count'] += 1;
                $map[$tokenDevice]['users'][] = $user->getID();
            } else {
                $map[$tokenDevice] = [
                    'count' => 1,
                    'users' => [$user->getID()]
                ];
            }

            // $badges = 0;
            // if (isset($user->badges)) {
            //     $badges = $user->badges;
            // }

            // $email = '';
            // if (isset($user->email)) {
            //     $email = $user->email;
            // }

            $results[] = [
                'uid' => $user->getID().'',
                'username' => $username,
                'devices' => $user->devices[0]['token'],
            ];
        }

        $finalMap = [];
        foreach( $map as $key=>$value ) {
            $numCount = $map[$key]['count'];
            if ($numCount > 1) {
                $finalMap = $map[$key];
            }
        }

        return ['status' => 1, 'message' => 'User Push Notifications with success!', 'data' => ['userdevices' => $finalMap]];
    }
    
    public function addDeviceToApplicationAction($uid, $post=false)
    {
        if ( !$uid )
            return ['status' => 6, 'message' => 'uid is required'];

        $input = $post!==false ? $post : $this->request->getPost();

        try {
            $user = Users::findById($uid);
        } catch ( \Exception $e ) {
            var_dump($e->getMessage());
            return;
        }

        if ( !$user )
            return ['status' => 4, 'message' => 'user doesn\'t exists!'];

        if ( !@$user->devices || !count($user->devices) )
            return ['status' => 5, 'message' => 'user doesn\'t have a valid device token!'];

        $this->config = $this->di->get('config');
        $sns = SnsClient::factory(array(
            'credentials' => [
                'key'     => $this->config->aws->key,
                'secret'  => $this->config->aws->secret,
            ],
            'region'  => $this->config->aws->region,
            'version' => $this->config->aws->version
        ));

        $udevices = [];
        foreach ( $user->devices as $pos=>$device ) {

            try {
                $res = $sns->createPlatformEndpoint([
                    'PlatformApplicationArn' => isset($input['topic']) ? $input['topic'] : 'arn:aws:sns:us-west-2:659866248444:app/APNS_SANDBOX/West-killer-push-app',
                    'Token' => $device['token'],
                    'CustomUserData' => $pos.'_'.$user->email. ' - '.(string) $user->_id,
                ]);

            } catch (AwsException $e) {
                //continue;
                return ['status' => 4, 'message' => $e->getAwsErrorMessage()];
            }

            if ( !$res )
                continue;
                //return ['status' => 2, 'device_token wasn\t added'];

            $udevices[] = ['token' => $device['token'], 'EndpointArn' => $res['EndpointArn']];

        }

        $user->devices = $udevices;
        $user->save();

        return ['status' => 1, 'message' => 'device_token added with success!', 'result' => ['EndpointArn' => $res['EndpointArn']]];
    }

    public function sendTestNotificationAction( )
    {
        $utoken = new UserToken();

        $token = $utoken->exists(['uid' => $this->uid]);

        if ( !(bool) $token && $this->tokenIsValid($token, (string) $this->uid) )
            return $token->token;

        $input = $this->request->getPost();

        try {
            $user = Users::findById($this->uid);
        } catch ( \Exception $e ) {
            var_dump($e->getMessage());
            return;
        }

        if ( !$user )
            return ['status' => 4, 'message' => 'user doesn\'t exists!'];

        $notification = new ExpoPushNotification(
            ine($input, 'device_token'),
            ine($input, 'title'),
            ine($input, 'body')
        );
        
        $badges = 1;
        // $new_message = $this->getNewMessagePush(ine($input, 'title'), ine($input, 'flashid'));
        // $messages = $this->getUserPushMessages($user, $new_message);

        $notification->setData([
            'title' => ine($input, 'title'), 
            'body' => ine($input, 'body'),
            'active' => true,
            'badges' => $badges,
            'uid' => $this->uid
        ]);

        $notification->setNumBadge($badges);
        
        $requests = new MRequests();

        $response = $requests->postRequest($notification->getJSONObj(), $this->expoEndPoint);
        
        if ($response['status'] == 1)
        {
            // $userUpdated = $this->updateUserBadgeAndMessages($user, $badges, $messages);
            
            // if ( $userUpdated->save() ) {
            return $response;
            // } else {
            //     return ['status' => 4, 'message' => 'Database error!'];
            // }

        }

        return $response;
    }

    protected function getNewMessagePush($title, $id)
    {
        $new_message = [];
        if ($id) {
            $new_message['id'] = $id;
        } else {
            $new_message['id'] = NULL;
        }

        $new_message['seen'] = false;

        return $new_message;
    }

    public function getBadgesCount($user)
    {
        $badges = 0;
        if ( isset($user->badges) ) {
            $badges = $user->badges;
        }
        
        $badges += 1;

        return $badges;
    }

    public function sendTestsTopicNotificationsAction( )
    {
        $utoken = new UserToken();
        $token = $utoken->exists(['uid' => $this->uid]);

        if ( !(bool) $token && $this->tokenIsValid($token, (string) $this->uid) )
            return $token->token;

        $input = $this->request->getPost();

        try {
            $user = Users::findById($this->uid);
        } catch ( \Exception $e ) {
            var_dump($e->getMessage());
            return;
        }

        if ( !$user )
            return ['status' => 4, 'message' => 'user doesn\'t exists!'];

        $result = $this->searchDeviceUsersWithTopic(ine($input, 'topic'));
        if ($result['status'] != 1 || count($result['result']) <= 0) {
            return ['status' => 4, 'message' => 'There are no devices found with the selected topic'];
        }

        $flashMessage = new FlashMessage(
            ine($input, 'title'), 
            ine($input, 'body'),
            ine($input, 'flashid')
        );

        $userDevices = $result['result'];

        $newList = [];
        for ($ndex = 0; $ndex < 1; $ndex++) {
            foreach($userDevices as $user) {
                $newList[] = $user;
            }
        }

        $pushNotifications = new MPushNotifications();
        $response = $pushNotifications->sendMultipleNotifications($newList, $flashMessage);

        if ( ine($input, 'repeats') )
        {
            $totalRepeats = ine($input, 'repeats');

            for($index = 0; $index < $totalRepeats; ++$index)
            {
                $responseRepeats = $pushNotifications->sendMultipleNotifications($userDevices, $flashMessage);
                
                $response['results']['total'] += $responseRepeats['results']['total'];
                $response['results']['success'] += $responseRepeats['results']['success'];
                $response['results']['error'] += $responseRepeats['results']['error'];
                $response['results']['totalrequests'] += $responseRepeats['results']['totalrequests'];
            }
        }

        return $response;
    }

    protected function sendMultiplePushNotifications($usersDevices, $title, $body, $id)
    {
        $maxLimitPerRequest = 100;
        $totalDevices = [];

        $sendResult = [];
        
        foreach ( $usersDevices as $user ) {

            $device = $user->devices[0];
            $notification = new ExpoPushNotification(
                $device['token'],
                $title,
                $body
            );

            $notification->setData(['title' => $title, 'body' => $body, 'active' => true, 'id' => $id]);

            $totalDevices[] = $notification->getJSONObj();

            if ( count($totalDevices) >= $maxLimitPerRequest ) {
                try {
                    $sendResult = $this->sendNotificationsLogic($totalDevices, $sendResult);
                } catch ( \Exception $e ) {
                    var_dump($e->getMessage());
                    return;
                }
            }
        }

        if (!empty($totalDevices))
        {
            try {
                $sendResult = $this->sendNotificationsLogic($totalDevices, $sendResult);
            } catch ( \Exception $e ) {
                var_dump($e->getMessage());
                return;
            }
        }

        return ['status' => 1, 'message' => 'Push Notifications successfully send', 'data' => $sendResult];

        // return ['status' => 2, 'message' => 'There was a problem sending the notifications'];
    }

    protected function sendNotificationsLogic($totalDevices, $sendResult)
    {
        $resultNotifications = $this->sendNotifications($totalDevices);
        $sendResult = $this->parseNotificationsResult($resultNotifications, $sendResult);
        unset($totalDevices);
        $totalDevices = array();
        return $sendResult;
    }

    protected function sendNotifications($totalDevices)
    {
        $requests = new MRequests();
        $response = $requests->postRequestJSON(json_encode($totalDevices), $this->expoEndPoint);

        return $response;
    }

    protected function parseNotificationsResult($resultObj, $sendResult)
    {
        $resultJSON = json_decode($resultObj['response']);
        foreach($resultJSON->data as $result)
        {
            $status = $result->status;
            $message = 'Ok';
            if (array_key_exists('message', $result))
            {
                $message = $result->message;
            }

            $sendResult[] = $this->newNotificationResult($status, $message);
        }

        return $sendResult;
    }

    protected function newNotificationResult($status, $message)
    {
        $statusNum = $status === 'ok' ? 1 : 0;
        return [
            'status'=> $statusNum,
            'message'=> $message
        ];
    }

    public function searchDeviceUsersWithTopic($topic)
    {
        $self = Users::find(
            [
                [
                    '$or' => [
                        ['topics' => ['$all' => [$topic]] ]
                    ],  
                ],
                'fields' => [
                    'devices' => true,
                    'username' => true,
                    'matricule' => true,
                    'badges' => true,
                    'push_messages' => true,
                    'email' => true
                ]
            ]
        );

        if ( $self === false )
            return ['status' => 2, 'message' => 'nothing found based on your query: '.$q];
        else
            return ['status' => 1, 'result' => $self];
    }

    public function searchMultipleDevices( )
    {
        $self = Users::find(
            [
                [
                    '$or' => [
                        ['topics' => ['$all' => ['flashes']] ]
                    ],  
                ],
                'fields' => [
                    'devices' => true,
                    'username' => true,
                    'matricule' => true,
                    'badges' => true,
                    'push_messages' => true,
                    'email' => true
                ]
            ]
        );

        if ( $self === false )
            return ['status' => 2, 'message' => 'nothing found based on your query: '.$q];
        else
            return ['status' => 1, 'result' => $self];
    }

    public function resetUsersPushAction()
    {
        $input = $this->request->getPost();

        $users = Users::find(
            [
                [
                    '$or' => [
                        ['topics' => ['$all' => ['flashes']] ]
                    ],  
                ],
            ]
        );

        $usersList = [];
        foreach($users as $user) {
            // if ($user->badges > 4) {
            //     $user->push_messages = [];
            //     $user->badges = 0;
            // }

            // $user->topics = ['general'];
            $user->push_messages = [
                [
                    "id" => "2791",
                    "flashid" => "4bfe3715d5a7a38debac487b9ea56ef3",
                    "seen" => false
                ],
                [
                    "id" => "2833",
                    "flashid" => "44bad61b7c2e1745d56fb645adf4cdf8",
                    "seen" => false
                ],
                [
                    "id" => "2867",
                    "flashid" => "f8dd27ff299cb62442c4798546675f23",
                    "seen" => false
                ],
                [
                    "id" => "2839",
                    "flashid" => "f8dd27ff299cb62442c4798546675f23",
                    "seen" => false
                ],
                [
                    "id" => "2839",
                    "flashid" => "f8dd27ff299cb62442c4798546675f23",
                    "seen" => false
                ]
            ];
            // $user->devices = [];

            // $usersList[] = $user;

            if ( $user->save() ) {
                $usersList[] = ['status' => 1, 'username' => $user->username, 'email' => $user->email];
            } else {
                $usersList[] = ['status' => -1, 'username' => $user->username, 'email' => $user->email];
            }

            /****** IMPORTANT */
            break;
        }

        return ['status' => 1, 'result' => $usersList, 'total' => count($usersList)];
    }

    public function registerNewDeviceAction( )
    {
        return $this->updateUserDeviceWithTopic();
    }
    
    public function removeUserDeviceAction( )
    {
        $input = $this->request->getPost();

        $deviceToken = ine($input, 'device_token');

        $result = $this->removeUserDeviceToken($deviceToken);

        return $result;
    }

    protected function updateUserDeviceWithTopic()
    {
        $utoken = new UserToken();
        $token = $utoken->exists(['uid' => $this->uid]);
        
        if ( !(bool) $token && $this->tokenIsValid($token, (string) $this->uid) )
            return $token->token;

        $input = $this->request->getPost();

        try {
            $user = Users::findById($this->uid);
        } catch ( \Exception $e ) {
            var_dump($e->getMessage());
            return;
        }

        if ( !$user )
            return ['status' => 4, 'message' => 'user doesn\'t exists!'];

        $topics = $this->getUserDeviceTopics($input);

        // $this->verifyUsersWithSameDeviceID($user, $input);

        // echo json_encode($user);

        $userUpdated = $this->insertDeviceTokenToUser($user, $input, $topics);

        if ( $userUpdated->save() ) {
            return ['status' => 1, 'message' => 'Device registered with success!'];
        } else {
            return ['status' => 4, 'message' => 'Database error!'];
        }
            // return ['status' => 4, 'message' => 'Database error!'];
    }

    public function updateUserPushNotificationsAction()
    {
        $utoken = new UserToken();

        $token = $utoken->exists(['uid' => $this->uid]);

        if ( !(bool) $token && $this->tokenIsValid($token, (string) $this->uid) )
            return $token->token;

        $input = $this->request->getPost();

        try {
            $user = Users::findById($this->uid);
        } catch ( \Exception $e ) {
            var_dump($e->getMessage());
            return;
        }

        if ( !$user )
            return ['status' => 4, 'message' => 'user doesn\'t exists!'];
            
        $itemID = ine($input, 'itemid');

        if ( !$itemID )
            return ['status' => 4, 'message' => 'item doesn\'t exists!'];

        $totalMessages = 0;
        if (isset($user->push_messages)) 
        {
            $totalMessages = count($user->push_messages);
        }

        $messages = $this->updateUserPushMessages( $user, $itemID );

        $totalBadgesRemoved = count($messages) - $totalMessages;
        
        $badges = $this->updateUserBadgesMinusOne( $user, $totalBadgesRemoved );

        $userUpdated = $this->updateUserBadgeAndMessages( $user, $badges, $messages );

        if ( $userUpdated->save() ) {
            return ['status' => 1, 'message' => 'Device registered with success!', 'data' => ['itemid' => $itemID]];
        } else {
            return ['status' => 4, 'message' => 'Database error!'];
        }
    }

    protected function getUserDeviceTopics($input)
    {
        $topicsString = ine($input, 'topics');
        $topics = [];

        foreach(explode(',', $topicsString) as $topic)
        {
            $topic = str_replace(' ', '', $topic);
            $topics[] = $topic;
        }
        
        $topics[] = 'general';
        // $topics[] = 'test';

        return $topics;
    }

    protected function verifyUsersWithSameDeviceID($user, $input)
    {
        $results = Users::find(
            [
                [
                    '$or' => [
                        ['devices' => [
                            'token' => ine($input, 'device_token') ?: '--'
                            ] 
                        ]
                    ]
                ]
            ]
        );

        if (!$results) {
            // echo "Nothing Found";
        } else {
            foreach($results as $userToUpdate) {
                if ($userToUpdate->getID() == $user->getID()) continue;
                $values = [
                    "devices" => [
                        ['token' => "notoken"]
                    ],
                    "badges" => isset($userToUpdate->badges) ? $userToUpdate->badges : 0,
                    "push_messages" => isset($userToUpdate->push_messages) ? $userToUpdate->push_messages : [],
                    "topics" => ["general"],
                    "updated_at" => $this->now,
                ];
    
                foreach ( $values as $key=>$val ) {
                    $userToUpdate->$key = $val;
                }

                if ( $userToUpdate->save() ) {
                 return ['status' => 1, 'message' => 'User Device updated with success!', 'username' => $userToUpdate->username];
                } else {
                    return ['status' => 4, 'message' => 'Database error!'];
                }
            }
        }
    }

    protected function removeUserDeviceToken($tokenDevice)
    {
        $results = Users::find(
            [
                [
                    '$or' => [
                        ['devices' => [
                            'token' => $tokenDevice
                            ] 
                        ]
                    ]
                ]
            ]
        );

        if (!$results) {
            return ['status' => 2, 'message' => 'Device Token does not exist'];
        } else {
            
            $userUpdated = false;
            $totalUsersUpdated = 0;
            foreach($results as $userToUpdate) {
                $values = [];
                $devices = [];
                foreach($userToUpdate->devices as $userDevice) {
                    if ($tokenDevice === $userDevice['token']) continue;
                    if ($userDevice['token'] === 'notoken') continue;
                    $devices[] = $userDevice;
                }

                if (count($devices) >= 1) {
                    $values = [
                        "devices" => $devices,
                        "updated_at" => date("Y-m-d h:i:s")
                    ];
                } else {
                    $values = [
                        "devices" => [
                            ['token' => "notoken"]
                        ],
                        "badges" => 0,
                        "push_messages" => [],
                        "topics" => ["general", "noflashes"],
                        "updated_at" => date("Y-m-d h:i:s")
                    ];
                }
    
                foreach ( $values as $key=>$val ) {
                    $userToUpdate->$key = $val;
                }

                if ( $userToUpdate->save() ) {
                    $userUpdated = true;
                    $totalUsersUpdated += 1;
                } else {
                    $userUpdated = false;
                }
            }

            return ['status' => 1, 'message' => 'Device removed with success', 'total' => $totalUsersUpdated];
        }
    }

    protected function insertDeviceTokenToUser($user, $input, $topics)
    {
        $push_messages = [];
        $badges = 0;
        $devices = [];
        if (isset($user->push_messages)) {
            $push_messages = $user->push_messages;
        }
        if (isset($user->badges)) {
            $badges = $user->badges;
        }
        if (isset($user->devices)) {
            $devices = $user->devices;
        }

        $deviceToken = ine($input, 'device_token') ? ['token' => ine($input, 'device_token') ] : ['token' => '' ]; 
        $devicesTotal = [];
        $devicesTotal[] = $deviceToken;
        foreach($devices as $value) {
            if ($deviceToken['token'] === $value['token']) continue;
            if ($value['token'] === 'notoken') continue;
            $devicesTotal[] = $value;
        }

        $values = [
            "devices" => $devicesTotal,
            "badges" => $badges,
            "push_messages" => $push_messages,
            "topics" => $topics,
            "updated_at" => date("Y-m-d h:i:s")
        ];

        foreach ( $values as $key=>$val ) {
            $user->$key = $val;
        }

        return $user;
    }

    protected function getUserPushMessages( $user, $new_message )
    {
        $messages = [];
        if (isset($user->push_messages)) 
        {
            $messages = $user->push_messages;
        }

        if ($new_message['id'] !== NULL) {
            $messages[] = $new_message;
        }

        return $messages;
    }

    protected function updateUserPushMessages( $user, $itemID )
    {
        if (!isset($user->push_messages)) 
        {
            return [];
        }

        $results = [];
        $messages = $user->push_messages;
        foreach ($messages as $item) {
            if ($item['id'] != $itemID) {
                $results[] = $item;
            }
        }

        return $results;
    }

    protected function updateUserBadgesMinusOne( $user, $total )
    {
        $badges = 0;
        if ( isset($user->badges) ) {
            $badges = $user->badges;
        }
        
        $badges += $total;

        if ($badges < 0) {
            $badges = 0;
        }

        return $badges;
    }

    protected function updateUserBadgeAndMessages( $user, $badges, $messages )
    {        
        $values = [
            "badges" => $badges,
            "push_messages" => $messages,
            "updated_at" => date('Y-m-d H:i:s'),
        ];

        foreach ( $values as $key=>$val ) {
            $user->$key = $val;
        }

        return $user;
    }

    private function verifyTheUserToken()
    {
        
    }

    public function publishAction($uid)
    {
        $self = new SNS();
        $input = $this->request->getPost();

        // has uid informed
        if ( $uid && $uid!='topic' ) {
            $input['uid'] = $uid;

            try {
                $user = Users::findById($input['uid']);
            } catch ( \Exception $e ) {
                var_dump($e->getMessage());
                return;
            }

            // uid is valid?
            if ( !$user )
                return ['status' => 4, 'message' => 'user doesn\'t exists!'];

            // uid has devices?
            if ( !@$user->devices || !count($user->devices) )
                return ['status' => 5, 'message' => 'user doesn\'t have a valid device token!'];

            $targets = $user->devices;

        // if don't have an uid, so inform a topic
        } elseif ( $uid=='topic' && !isset($input['topic']) ) {
            return ['status' => 3, 'message' => 'inform a uid OR a ARNTopic before send an message!'];
        } else {
            $targets[] = ['topic' => $input['topic']];
        }

        //  message already sent?
        if ( 1==2 && $self->exists($input) )
            return ['status' => 2, 'message' => 'this message already was sent recently!'];

        $sns = SnsClient::factory(array(
            'credentials' => [
                'key'     => $this->config->aws->key,
                'secret'  => $this->config->aws->secret,
            ],
            'region'  => $this->config->aws->region,
            'version' => $this->config->aws->version
        ));

        $suc = $err = [];
        foreach ( $targets as $i=>$target ) {

            // send the message
            try {

                $data = [
                    'Message' => $input['message'],
                    'Subject' => $input['subject'],
                    'MessageStructure' => 'string'
                ];

                if ( isset($target['topic']) ) {
                    $input['TopicArn'] = $data['TopicArn'] = $target['topic'];
                    $target = $target['topic'];
                }

                if ( isset($target['token']) ) {
                    $input['TargetArn'] = $data['TargetArn'] = $target['EndpointArn'];
                    $target = $target['token'];
                }

                $res = $sns->publish($data);

            } catch (AwsException $e) {
                return ['status' => 4, 'message' => $e->getAwsErrorMessage()];
            }

            if ( isset($res['MessageId']) ) {
                $input[$i]['messageId'] = $res['MessageId'];
                $suc[$i] = $target;
            } else {
                $err[$i] = $target;
            }

        }

        $result = [];
        if ( count($suc) ) {
            $code = 1;
            $message = 'Message successfuly sent to '. count($suc);
            $result['success'] = $suc;
            $input['success_count'] = count($suc);
        }

        if ( count($err) ) {
            $code = 2;
            $message = count($err). ' errors';
            $result['error'] = $err;
            $input['errors_count'] = count($err);
        }

        $self = $this->insert($self, $input);
        $self->save();

        return ['status' => $code, 'message' => $message, 'result' => $result];
    }

    protected function insert($self, $input)
    {
        $exists = (bool) @$self->_id;

        // create safe array of values
        $values = [
            "uid" => ine($input, 'uid') ?: '',
            "TargetArn" => ine($input, 'TargetArn') ?: '',
            "TopicArn" => ine($input, 'TopicArn') ?: '',
            "subject" => ine($input, 'subject') ?: '',
            "content" => ine($input, 'content') ?: '',
            "success_count" => ine($input, 'success_count') ?: 0,
            "errors_count" => ine($input, 'errors_count') ?: 0,
        ];

        if ( !$exists )
            $values['created_at'] = date('Y-m-d H:i:s');

        // insert those values into this object
        foreach ( $values as $key=>$val ) {
            $self->$key = $val;
        }

        return $self;
    }

}
