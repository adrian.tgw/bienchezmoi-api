<?php 
declare(strict_types=1);
namespace Controllers;

use Phalcon\Mvc\Dispatcher;

use ParagonIE\Halite\HiddenString;
use ParagonIE\Halite\Password;

use Firebase\JWT\JWT;

use Models\Log;
use Models\UserToken;

class LogController extends ControllerBase
{
    public function initialize()
    {
        parent::initialize();
    }

    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        
        $allowedActionsWithoutToken = ["downloadEvents"];
        
        if (in_array($dispatcher->getActionName(), $allowedActionsWithoutToken)) 
        {
            $this->isProtectedByToken = false;
        }

        parent::beforeExecuteRoute($dispatcher);

    }

    public function newEventsAnonymAction( )
    {
        try {

            $uid = "anonym";
            
            if (!file_exists(BASE_PATH."/storage/logger")) {
                mkdir(BASE_PATH."/storage/logger", 0777, true);
            }
    
            $fileName = BASE_PATH."/storage/logger/" . $uid . "_01.json";
            if ( !file_exists($fileName) ) {
                file_put_contents($fileName, ( json_encode( array() ) )  );
            }
    
            $events = json_decode($this->request->getPost('events'));
            $fileContent = file_get_contents($fileName);
            $content = json_decode( $fileContent );
            foreach($events as $event) 
            {
                $content[] = $event;
            }
    
            $objToSave = json_encode( $content ) ;
            
            file_put_contents($fileName, $objToSave );

            $logModel = new Log();
            if ( !$logModel->existsAnonym($uid) )
            {
                $logModel->registerNewAnonym($uid);
            }
    
            return ['status' => 1, 'message' => 'Success!'];

        } catch ( \Exception $e ) {

            return ['status' => 2, 'message' => $e->getMessage() ];

        }
    }
    
    public function newEventsAction( )
    {
        try {

            $uid = $this->uid;
            
            if (!file_exists(BASE_PATH."/storage/logger")) {
                mkdir(BASE_PATH."/storage/logger", 0777, true);
            }
    
            $fileName = BASE_PATH."/storage/logger/" . $uid . "_01.json";
            if ( !file_exists($fileName) ) {
                file_put_contents($fileName, ( json_encode( array() ) )  );
            }
    
            $events = json_decode($this->request->getPost('events'));
            $fileContent = file_get_contents($fileName);
            $content = json_decode( $fileContent );
            foreach($events as $event) 
            {
                $content[] = $event;
            }
    
            $objToSave = json_encode( $content ) ;
            
            file_put_contents($fileName, $objToSave );

            $logModel = new Log();
            if ( !$logModel->exists($uid) )
            {
                $logModel->registerNew($uid);
            }
    
            return ['status' => 1, 'message' => 'Success!'];

        } catch ( \Exception $e ) {

            return ['status' => 2, 'message' => $e->getMessage() ];

        }
    }

    public function getEventsAction( $count, $type )
    {
        $type = intval($type);
        try {
            $logModel = new Log();
            $logList = $logModel->getAvailable();
            $items = array();
            foreach($logList as $log)
            {
                $uid = $log->getUid();
                $fileName = BASE_PATH."/storage/logger/" . $log->getUid() . "_01.json";
                if ( file_exists($fileName) ) {
                    $contentFile = file_get_contents($fileName);
                    $contentString = stripslashes($contentFile);
                    $content = json_decode($contentString);
                    if ($content === null) {
                        //echo $contentFile;
                        echo json_encode(json_last_error_msg(), 128);
                        echo "\n";
                    } else {
                        foreach($content as $item) {
                            $items[] = $item;
                        }
                    }
                }
            }

            usort($items, function($a, $b) {
                return strtotime( $b->created_at ) - strtotime( $a->created_at );
            });

            $list = array();
            $limit = 0;
            foreach($items as $item)
            {
                if ($limit >= $count) break;
                if ( !isset($item->type) ) continue;

                if (!isset($item->ref) ) 
                {
                    $item->ref = " ";
                }

                if (!isset($item->publishid) ) 
                {
                    $item->publishid = " ";
                }
                
                
                if ($type !== 0) {
                    if ($type === $item->type) {
                        $list[] = $item;
                        $limit++;
                    }
                } else {
                    $list[] = $item;   
                    $limit++;
                }
            }

            return ['status' => 1, 'message' => 'Success!', 'result' => $list];
        } catch ( \Exception $e ) {

            return ['status' => 2, 'message' => $e->getMessage() ];

        }
    }

    public function downloadEventsAction( $token, $count, $type )
    {
        $utoken = new UserToken();

        try {
            $decodedToken = $this->decodeToken($token);
            if (!$decodedToken || !$decodedToken->uid)
            {
                $response = new \Phalcon\Http\Response();
                $response->setStatusCode(401, "Unauthorized");
                $response->setContent(json_encode(['code' => 401, 'message' => "Unauthorized" ]));
                $response->setContentType("application/json", "UTF-8");

                $response->send();
                exit;
                
            }

            if ( !$utoken->exists(["uid" => $decodedToken->uid]) )
            {
                $response = new \Phalcon\Http\Response();
                $response->setStatusCode(401, "Unauthorized");
                $response->setContent(json_encode(['code' => 401, 'message' => "Unauthorized" ]));
                $response->setContentType("application/json", "UTF-8");

                $response->send();
                exit;
            }

        }  catch ( \Exception $e ) {

            $response = new \Phalcon\Http\Response();
            $response->setStatusCode(401, "Unauthorized");
            $response->setContent(json_encode(['code' => 401, 'message' => "Unauthorized" ]));
            $response->setContentType("application/json", "UTF-8");

            $response->send();
            exit;
        }

        try {
            $type = intval($type);
            $logModel = new Log();
            $logList = $logModel->getAvailable();
            $items = array();
            foreach($logList as $log) 
            {
                $fileName = BASE_PATH."/storage/logger/" . $log->getUid() . "_01.json";
                if ( file_exists($fileName) ) {
                    $content = json_decode( file_get_contents($fileName, false) );
                    foreach($content as $item) {
                        if (!isset($item->ref) ) 
                        {
                            $item->ref = " ";
                        }

                        if (!isset($item->publishid) ) 
                        {
                            $item->publishid = " ";
                        }
                        
                        $items[] = $item;
                    }
                }
            }

            usort($items, function($a, $b) {
                return strtotime( $b->created_at ) - strtotime( $a->created_at );
            });

            $list = array();
            $limit = 0;
            foreach($items as $item)
            {
                if ($limit >= $count) break;
                if ( !isset($item->type) ) continue;
                
                if ($type !== 0) {
                    if ($type === $item->type) {
                        $list[] = $item;
                        $limit++;
                    }
                } else {
                    $list[] = $item;   
                    $limit++;
                }
            }

            //$out = null;
            $out = fopen('php://output', 'w');
            //echo json_encode( array_keys( (array) $list[0] ), 128);
            fputcsv($out,  array("Bouton", "Demandeur", "Type", "Annonceur", "Titre de l'annonce", "Numèro Professionnel", "Identifiant d'annonce", "Créé à") );

            $types = array("", "Recherche", "Voir annonce", "Email", "Téléphone", "-");
            $whats = array("", "Louer", "Achat");
            foreach($list as $item)
            {
                $what = $whats[$item->what];
                $element = [
                    "type" => $types[$item->type],
                    "user" => $item->user,
                    "what" => $what,
                    "owner" => $item->owner,
                    "title" => $item->title,
                    "ref" => $item->ref,
                    "publishid" => $item->publishid,
                    "created_at" => date("Y-m-d h:i:s", strtotime( $item->created_at ))
                ];
                fputcsv($out, $element);
            }
        
            fclose($out);

            $response = new \Phalcon\Http\Response();
            $response->setStatusCode(200, "OK");
            //$response->setHeader("Access-Control-Allow-Origin", "http://appbridor.com");
            $response->setHeader("Access-Control-Allow-Origin", "*");
            $response->setHeader('Content-Disposition', 'attachement; filename="data.csv"');
            $response->setContentType("application/force-download", "UTF-8");
            //$response->setContentType("text/xml", "UTF-8");

            /*
            $answer = "<?xml version='1.0' standalone='yes'?>";
            $answer .= "<peliculas></peliculas>";
            $response->setContent($answer);
            */
            $response->setContent($out);
            $response->send();
            exit;

        } catch ( \Exception $e ) {

            return ['status' => 2, 'message' => $e->getMessage() ];

        }
    }

}

