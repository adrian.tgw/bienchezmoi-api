<?php
declare(strict_types=1);
namespace Controllers;

class IndexController extends ControllerBase
{
    protected $isProtectedByToken = false;

    public function indexAction()
    { }

    public function route401Action()
    {
        $data = $this->dispatcher->getParam('data');
        $this->response->setStatusCode(401, 'Unauthorized');
        $this->response->setContentType("application/json", "UTF-8");
        $this->response->setJsonContent( $data );
        $this->response->send();
        exit;
    }
    
    public function route404Action()
    {
        $this->response->setStatusCode(404, 'Not Found');
        $this->response->setContentType("application/json", "UTF-8");
        $this->response->setJsonContent([
            "message" => "Not Found",
            "status" => 404
        ]);
        $this->response->send();
        exit;
    }
}

