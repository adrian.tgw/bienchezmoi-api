<?php 
declare(strict_types=1);
namespace Controllers;

use Phalcon\Mvc\Dispatcher;

use Presenters\UsersPresenter;
use Models\Users;
use Models\UserProfile;
use Models\UserToken;
use Models\UserConfirmation;
use Models\UserRecovery;

use Services\Email;
use Services\S3;
use Services\MRequests;

use ParagonIE\Halite\HiddenString;
use ParagonIE\Halite\Password;

date_default_timezone_set('UTC');

class UsersController extends ControllerBase
{
    public function initialize()
    {
        parent::initialize();
    }

    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        $allowedActionsWithoutToken = [
            "new",
            "newSocial",
            "newAdmin",
            "verifyUser",
            "newProfessional",
            "confirmationEmail",
            "passwordrecovery",
            "passwordrecoveryupdate"
        ];
        
        if (in_array($dispatcher->getActionName(), $allowedActionsWithoutToken)) 
        {
            $this->isProtectedByToken = false;
        }

        parent::beforeExecuteRoute($dispatcher);

    }

    public function getAllAction( )
    {
        $user = new Users();
        $usersList = $user->getAllUsers();
        return ['status' => 1, 'message' => 'success!', 'users' => $usersList];
    }

    public function getAllProfileAction()
    {
        $user = new UsersPresenter( $this->config );
        return $user->getUsersAndProfiles();
    }

    public function getUserProfileAction($email)
    {
        $user = new Users();
        if (!$user->existsLocal($email)) {
            return ['status' => 0];
        } 

        $user = $user->getLocalUser($email);
        $userPresenter = new UsersPresenter( $this->config );
        return $userPresenter->getUserAndProfile((string) $user->getId());
    }

    public function verifyUserAction( $email )
    {
        $user = new Users();
        if ($user->existsLocal($email)) {
            $userFound = $user->getLocalUser($email);
            return ['status' => 1, 'result' => [ "version" => $userFound->version ]];
        } else {
            return ['status' => 0];
        }
    }

    public function getUserAction($email)
    {
        $user = new Users();
        $userFound = $user->getLocalUser($email);
        if ($userFound) {
            return ['status' => 1, 'message' => 'success!', 'user' => $userFound];
        } else {
            return ['status' => 2, 'message' => 'User not found!'];
        }
    }

    public function getMeAction()
    {   
        $user = new UsersPresenter( $this->config );
        return $user->getUserAndProfile($this->uid);
    }

    public function confirmationEmailAction($id)
    {
        $userConfirmation = new UserConfirmation();
        try {
            if ( $userConfirmation->exists($id) )
            {
                $userConfirmation = $userConfirmation->getToken($id);
            }
            else 
            {
                return ['status' => 2, 'message' => 'User already confirmed!'];
            }
        } catch ( \Exception $e ) {
            return ['status' => 4, 'message' => 'server error!'];
        }

        $user = new Users();
        try {
            $userFound = $user->getLocalUserById($userConfirmation->uid);
        } catch ( \Exception $e ) {
            return ['status' => 4, 'message' => 'server error!'];
        }

        if ( !$userFound || $userFound == null ) {
            return ['status' => 2, 'message' => 'user doesn\'t exist!'];
        }

        if ( $userFound->status != 2 ) {
            return ['status' => 2, 'message' => 'User already confirmed!'];
        }

        $userUpdated = $user->setUserConfirmed( $userFound );
        if (!$userUpdated) {
            return ['status' => 4, 'message' => 'server error!'];
        }

        $userConfirmation->update();

        return ['status' => 1, 'message' => 'User updated!'];
    }

    public function updateStatusAction()
    {
        $userPresenter = new UsersPresenter( $this->config );
        $input = $this->request->getPost();

        return $userPresenter->setUserActiveState( $input["uid"], $input["active"] === 'true' ? true : false );
    }

    public function updatePasswordAction()
    {
        $userPresenter = new UsersPresenter( $this->config );
        $input = $this->request->getPost();

        return $userPresenter->setUserPassword( $this->uid, $input, $this->secretKey );
    }

    public function updateProfileAction()
    {
        $userPresenter = new UsersPresenter( $this->config );
        $input = $this->request->getPost();

        return $userPresenter->updateProfile( $this->uid, $input );
    }

    public function updateInfoAction()
    {
        $userPresenter = new UsersPresenter( $this->config );
        $input = $this->request->getPost();

        return $userPresenter->setUserInfo( $this->uid, $input );
    }

    public function showAction($id)
    {
        try {
            if ( $id=='me' )
                $id = $this->uid;

            $self = Users::findById($id);
        } catch ( \Exception $e ) {
            return ['status' => 2, 'message' => 'user not found'];
        }

        return $self;
    }

    public function newAction()
    {
        $userPresenter = new UsersPresenter( $this->config );
        $input = $this->request->getPost();

        $userModel = new Users();
        if ( $userModel->exists($input) )
        return ['status' => 2, 'message' => 'username or email already exist!'];
        
        return $userPresenter->newUserIndividual( $input, $this->secretKey );
    }

    public function newSocialAction()
    {
        $userPresenter = new UsersPresenter( $this->config );
        $input = $this->request->getPost();

        $userModel = new Users();
        if ( $userModel->exists($input) )
        return ['status' => 2, 'message' => 'username or email already exist!'];
        
        return $userPresenter->newUserSocial( $input, $this->secretKey );
    }

    public function newProfessionalAction()
    {
        $userPresenter = new UsersPresenter( $this->config );
        $input = $this->request->getPost();

        $userModel = new Users();
        if ( $userModel->exists($input) )
            return ['status' => 2, 'message' => 'username or email already exist!'];

        return $userPresenter->newUserProfessional( $input, $this->secretKey );
    }

    public function setProfileAction()
    {
        $userPresenter = new UsersPresenter( $this->config );
        $input = $this->request->getPost();

        return $userPresenter->setUserProfile( (string) $this->uid, $input, $this->secretKey );
    }

    public function newAdminAction()
    {
        return ['status' => -1, 'message' => 'NO!'];
        $user = new Users();
        $input = $this->request->getPost();

        if ( $user->exists($input) )
            return ['status' => 2, 'message' => 'username or email already exists!'];

        $user = $user->registerNewWithPassword($input, $this->secretKey);
        if ( $user ) {
            $user->permissions = 5;
            $user->status = 1;
            $user = $user->updateUser($user);
            return ['status' => 1, 'message' => 'register success!'];
        }
        else
        {
            return ['status' => 4, 'message' => 'database error!'];
        }
    }

    public function deleteUserAction($id)
    {
        $utoken = new UserToken();
        $token = $utoken->exists(['uid' => $this->uid]);

        if ( !(bool) $token && $this->tokenIsValid($token, (string) $this->uid) )
            return $token->token;

        try {
            $self = Users::findById($id);
        } catch ( \Exception $e ) {
            return ['status' => 4, 'message' => 'server error!'];
        }

        if ( !$self )
            return ['status' => 2, 'message' => 'user does not exist!'];

        $self->delete();

        $userProfile = new UserProfile();
        $userProfileObj = $userProfile->getByUid( (string) $id );
        if ($userProfileObj) {
            $userProfileObj->delete();
        }
        
        return ['status' => 1, 'message' => 'user '.$self->email.' was removed!'];
    }

    public function passwordrecoveryAction($email)
    {
        $self = new Users();
        try {
            $userExists = $self->existsLocal($email);
            if ( !$userExists || $userExists == null ) {
                return ['status' => 2, 'message' => 'email "'.$email.'" doesn\'t exist!'];
            }
            $self = $self->findWithEmail($email);
        } catch ( \Exception $e ) {
            return ['status' => 4, 'message' => 'server error!'];
        }

        $userRecovery = new UserRecovery();
        $userRecovery->newToken( (string) $self->_id );

        $notify = new Email();
        return $notify->sendRecoveryEmail(
            $self->email, 
            $this->config->application['name'], 
            'emails/users/password-recovery', 
            $this->config->email->replyTo, 
            $self,
            (string) $userRecovery->_id
        );
    }

    public function passwordrecoveryupdateAction($id)
    {
        $userRecovery = new UserRecovery();
        try {
            if ( $userRecovery->exists($id) )
            {
                $userRecovery = $userRecovery->getToken($id);
            }
            else 
            {
                return ['status' => 2, 'message' => 'Recovery password request do not exist!'];
            }
        } catch ( \Exception $e ) {
            return ['status' => 4, 'message' => 'server error!'];
        }

        if ( $userRecovery->status != 1 ) {
            return ['status' => 2, 'message' => 'Recovery password request is not longer available!'];
        }

        $user = new Users();
        $userFound = null;
        try {
            $userFound = $user->getLocalUserById($userRecovery->uid);
        } catch ( \Exception $e ) {
            return ['status' => 4, 'message' => 'server error!'];
        }

        if ( !$userFound || $userFound == null ) {
            return ['status' => 2, 'message' => 'user doesn\'t exist!'];
        }

        $input = $this->request->getPost();
        if ( !isset($input['passwd']) ) {
            return ['status' => 3, 'message' => 'No password found!'];
        }

        $passwd = $input['passwd'];
        $userUpdated = $userFound->updateUserPassword($passwd, $this->secretKey);

        if ($userUpdated) {
            $userRecovery->update();
            return ['status' => 1, 'message' => 'Password Successful Updated!'];
        } else {
            return ['status' => 2, 'message' => 'Password not updated!'];
        }
    }

    public function appVersionSetAction()
    {
        $utoken = new UserToken();

        $token = $utoken->exists(['uid' => $this->uid]);

        if ( !(bool) $token && $this->tokenIsValid($token, (string) $this->uid) )
            return $token->token;
        
        try {
            $user = Users::findById($this->uid);
        } catch ( \Exception $e ) {
            var_dump($e->getMessage());
            return;
        }

        $input = $this->request->getPost();

        $appVersion = 0;
        if (isset($input['appversion'])) {
            $appVersion = floatval( ine($input, 'appversion') );
        } else {
            return ['status' => 2, 'message' => 'app version does not exist!'];
        }

        $user->appVersion = $appVersion;

        if ( $user->save() ) {
            return ['status' => 1, 'message' => 'user updated with success!'];
        } else
            return ['status' => 4, 'message' => 'database error!'];
            
    }

    public function settingsAction()
    {
        $utoken = new UserToken();

        $token = $utoken->exists(['uid' => $this->uid]);

        if ( !(bool) $token && $this->tokenIsValid($token, (string) $this->uid) )
            return $token->token;
        
        try {
            $user = Users::findById($this->uid);
        } catch ( \Exception $e ) {
            var_dump($e->getMessage());
            return;
        }

        $input = $this->request->getPost();

        $notification = false;
        if (isset($input['notifications'])) {
            $notification = intval( ine($input, 'notifications') ) === 1 ? true : false;
        } else {
            return ['status' => 2, 'message' => 'time does not exist!'];
        }

        $user->notification = $notification;

        if ( $user->save() ) {
            return ['status' => 1, 'message' => 'user updated with success!'];
        } else
            return ['status' => 4, 'message' => 'database error!'];
        
    }

}
