<?php 
declare(strict_types=1);
namespace Models;

use Phalcon\Mvc\MongoCollection;
use Phalcon\Db\Adapter\MongoDB\Operation;

class Location extends MongoCollection
{
    public $parentid;
    public $villes;
    public $communes;
    public $quartiers;

    public $type;
    public $value;

    public $status;

    public function getSource()
    {
        return 'location';
    }

    public function initialize()
    {
        $this->parentid = "";

        $this->pays = 0;
        $this->villes = 0;
        $this->communes = 0;
        $this->quartiers = 0;

        $this->value = "";
        $this->type = 0;

        $this->status = 0;
    }

    public function getByIdObj($id) {
        return Location::findById($id);
    }

    public function getById($id)
    {
        return $this->modelFormat(Location::findById($id));
    }

    public function getAvailable()
    {
        $filter = [
            'conditions' => [
                'status' =>  1,
                'type' => [ '$exists' => true ]
            ]
        ];

        $results = $this->find($filter);
        if (!$results) {
            return array();
        }

        return $this->modelFormatOnList($results);
    }

    public function getAllByType($type)
    {
        $filter = [
            'conditions' => [
                'type' =>  ['$eq' => intval($type)]
            ]
        ];
        $results = $this->find($filter);
        if (!$results) {
            return array();
        }

        return $this->modelFormatOnList($results);
    }

    public function getAvailableByType($type)
    {
        $filter = [
            'conditions' => [
                'status' => 1,
                'type' =>  ['$eq' => intval($type)]
            ]
        ];
        $results = $this->find($filter);
        if (!$results) {
            return array();
        }

        return $this->modelFormatOnList($results);
    }

    public function getAllByParent($id)
    {

        $filter = [
            'conditions' => [
                'parentid' =>  new \MongoDB\BSON\ObjectId($id)
            ]
        ];
        $results = $this->find($filter);
        if (!$results) {
            return array();
        }

        return $this->modelFormatOnList($results);
    }

    public function getAvailableByParent($id)
    {

        $filter = [
            'conditions' => [
                'status' => 1,
                'parentid' =>  new \MongoDB\BSON\ObjectId($id)
            ]
        ];
        $results = $this->find($filter);
        if (!$results) {
            return array();
        }

        return $this->modelFormatOnList($results);
    }

    public function newItemByType($type, $parentType, $item) 
    {
        $newItem = [];
        $newItem["value"] = $item["value"];
        $newItem["type"] = $type;
        $newItem["parentid"] = "";
        if ($item["parent"] !== "") {
            $items = $this->getAllByType( $parentType );
            foreach($items as $itemFound) {
                if ($itemFound["value"] === $item["parent"]) {
                    $newItem["parentid"] = $itemFound["id"];
                }
            }
        }

        //echo json_encode($newItem, JSON_PRETTY_PRINT);
        
        return $this->registerNew($newItem);
    }

    public function registerNew($input)
    {
        $self = new Location();
        $self->initialize();

        $type = intval($input["type"]);
        $self->type = $type;
        $self->value = $input["value"];

        $self->status = 1;

        if ($type === 1) {
            $self->pays = 1;
        }

        if ($type === 2) {
            $self->villes = 1;
        }

        if ($type === 3) {
            $self->communes = 1;
        }

        if ($type === 4) {
            $self->quartiers = 1;
        }

        $parentid = $input["parentid"];
        if ($parentid && $parentid !== "") {
            $self->parentid = new \MongoDB\BSON\ObjectId($parentid);
        }

        $self->created_at = date("Y-m-d h:i:s");
        
        if ( $self->save() )
            return $self;
            
        return false;
    }

    public function update($input)
    {
        $this->updated_at = date("Y-m-d h:i:s");
        $this->value = $input["value"];
        $this->status = intval($input["status"]);
        
        if ( $this->save() )
            return $this;
        
        return false;
    }
    
    // Model Value Interaction
    public function modelFormatOnList($list)
    {
        return array_map(
            function($item)
        {
            return $this->modelFormat($item);
        }, 
        $list);
    }

    public function modelFormat($item)
    {
        return [
            'id' => (string) $item->_id,
            'parentid' => (string) $item->parentid,
            'value' => $item->value,
            'type' => $item->type,
            'status' => $item->status
        ];
    }

}
