<?php
namespace Models;

use Phalcon\Mvc\MongoCollection;
use Phalcon\Db\Adapter\MongoDB\Operation;

class UserToken extends MongoCollection
{
    public $token, $uid;
    public $anonymous = false;
    
    public function getSource()
    {
        return 'user_token';
    }

    public function exists($input, $by='uid')
    {
        $exists = $this->find(
            [
                'conditions' => [
                    $by => $input[$by]
                ],
                'sort' => [
                    'created_at' => -1
                ],
                'limit' => 1
            ]
        );

        return (bool) $exists ? $exists[0] : false;
    }

    public function getToken($input) 
    {
        $tokenFound = $this->exists($input);
        if ($tokenFound) {
            return $tokenFound;
        }
        
        return new UserToken();
    }

}
