<?php
//namespace App;
namespace Models;

class ExpoPushNotification
{
    public $to;
    public $title;
    public $body;
    public $data;
    public $sound = "default";
    public $channelId = "default";
    public $badge = 1;

    public $json = array();

    function __construct($to, $title, $body) 
    {
        $this->title = $title;
        $this->body = $body;
        $this->to = $to;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function setNumBadge($badge)
    {
        $this->badge = $badge;
    }

    public function setSound($sound)
    {
        $this->sound = $sound;
    }

    public function setDeviceToken($device)
    {
        $this->to = $to;
    }

    public function getJSONObj()
    {
        $result = [];

        if ($this->to) {
            $result['to'] = 'ExponentPushToken[' . $this->to . ']';
        }

        if ($this->data) {
            $result['data'] = $this->data;
        }

        if ($this->title) {
            $result['title'] = $this->title;
        }

        if ($this->body) {
            $result['body'] = $this->body;
        }

        if ($this->sound) {
            $result['sound'] = $this->sound;
        }

        if ($this->badge) {
            $result['badge'] = $this->badge;
        }

        if ($this->channelId) {
            $result['channelId'] = $this->channelId;
        }

        return $result;
    }
}
