<?php 
declare(strict_types=1);
namespace Models;

use Phalcon\Mvc\MongoCollection;
use Phalcon\Db\Adapter\MongoDB\Operation;

class Publish extends MongoCollection
{
    public $uid;
    public $short;
    public $large;
    public $pie;
    public $cham;
    public $surf;
    public $price;
    public $loc;
    public $ville = "";
    public $pays = "";
    public $quartier = "";
    public $commune = "";
    public $what;
    public $habitation;
    public $location;
    public $photos;

    public $status;

    public function getSource()
    {
        return 'publish';
    }

    public function initialize()
    {
        $this->uid = "";

        $this->short = "";
        $this->large = "";
        $this->pie = "";
        $this->cham = "";
        $this->surf = "";
        $this->price = "";
        $this->loc = "";
        $this->pays = "";
        $this->ville = "";
        $this->commune = "";
        $this->quartier = "";
        $this->what = 0;
        $this->habitation = array();
        $this->location = array();
        $this->photos = array();

        $this->status = 0;
    }

    public function getAvailable()
    {
        $filter = ['conditions' => [], 'sort' => []];
        $filter['conditions']['status'] = 1;
        
        try
        {
            $items = Publish::find($filter);
            return $items;
        } catch ( \Exception $e ) {
            return false;
        }
    }

    public function getAll()
    {
        $filter = ['conditions' => []];

        try
        {
            $items = Publish::find($filter);
            return $items;
        } 
        catch ( \Exception $e ) 
        {
            return false;
        }
    }

    public function getByUid($uid)
    {
        $filter = ['conditions' => ['uid' => new \MongoDB\BSON\ObjectId($uid) ]];
        return $this->findFirst($filter);
    }

    public function getAllByUid($uid)
    {
        $filter = ['conditions' => ['uid' => new \MongoDB\BSON\ObjectId($uid) ]];
        return $this->find($filter);
    }

    public function getById($id)
    {
        return Publish::findById($id);
    }

    public function registerNew($uid, $input)
    {
        $self = new Publish();
        $self->initialize();
        $self = $this->setPublishValues( $self, $input );
        $self->uid = new \MongoDB\BSON\ObjectId($uid);
        $self->created_at = date("Y-m-d h:i:s");
        $self->status = 5;

        if ( $self->save() )
            return $self;
            
        return false;
    }

    public function registerNewForProfessional($uid, $input)
    {
        $self = new Publish();
        $self->initialize();
        $self = $this->setPublishValues( $self, $input );
        $self->uid = new \MongoDB\BSON\ObjectId($uid);
        $self->created_at = date("Y-m-d h:i:s");
        $self->status = 1;

        if ( $self->save() )
            return $self;
            
        return false;
    }

    private function setPublishValues( $self, $input )
    {
        $self->short = $input["short"];
        $self->large = $input["large"];
        $self->pie = intval($input["pie"]);
        $self->cham = intval($input["cham"]);
        $self->surf = floatval($input["surf"]);
        $self->price = floatval($input["price"]);
        $self->pays = $input["pays"];
        $self->ville = $input["ville"];
        $self->commune = $input["commune"];
        $self->quartier = $input["quartier"];
        $self->what = intval($input["what"]);
        $self->habitation = json_decode($input["habitation"], true);
        $self->location = json_decode($input["location"], true);

        return $self;
    }

    public function setPhotos( $photos )
    {
        $this->photos = $photos;
    }

    public function setPublishActive($id)
    {
        $self = $this->getById($id);
        $self->activated_at = date("Y-m-d h:i:s");
        $self->status = 1;

        if ( $self->save() )
            return $self;
            
        return false;
    }
    
    public function updatePublish($id, $input)
    {
        $self = $this->getById($id);
        $self = $this->setPublishValues( $self, $input );
        $self->updated_at = date("Y-m-d h:i:s");
        $self->status = intval( $input["status"] );

        if ( $self->save() )
            return $self;
            
        return false;
    }

    public function updatePublishStatus($id, $status)
    {
        $self = $this->getById($id);
        if ($self->status === 5) return false;
        
        $self->status = intval( $status );
        $self->updated_at = date("Y-m-d h:i:s");

        if ( $self->save() )
            return $self;
        
        return false;
    }

    public function update()
    {
        $this->updated_at = date("Y-m-d h:i:s");
        
        if ( $this->save() )
            return $this;
        
        return false;
    }
    
    // Model Value Interaction
    public function modelFormatOnList($list)
    {
        return array_map(
            function($item)
        {
            return $this->modelFormat($item);
        }, 
        $list);
    }

    public function modelFormat($item)
    {
        return [
            'id' => (string) $item->_id,
            'uid' => (string) $item->uid,
            'short' => $item->short,
            'large' => $item->large,
            'pie' => $item->pie,
            'cham' => $item->cham,
            'surf' => $item->surf,
            'price' => $item->price,
            'pays' => $item->pays,
            'ville' => $item->ville,
            'commune' => $item->commune,
            'quartier' => $item->quartier,
            'what' => $item->what,
            'habitation' => $item->habitation,
            'location' => $item->location,
            'photos' => $item->photos,
            'status' => $item->status,
            'created' => @$item->created_at,
        ];
    }

    // Model Value Interaction
    public function modelFormatOnListForSearch($list)
    {
        return array_map(
            function($item)
        {
            return [
                'id' => (string) $item->_id,
                'uid' => (string) $item->uid,
                'short' => $item->short,
                'large' => $item->large,
                'pie' => $item->pie,
                'cham' => $item->cham,
                'surf' => $item->surf,
                'price' => $item->price,
                'pays' => @$item->pays,
                'ville' => @$item->ville,
                'commune' => @$item->commune,
                'quartier' => @$item->quartier,
                'what' => $item->what,
                'habitation' => (array) $item->habitation,
                'location' => (array) $item->location,
                'photos' => (array) $item->photos,
                'status' => $item->status,
                'created' => @$item->created_at,
                'user' => [
                    'email' => $item->user["email"],
                    'name' => $item->user["name"],
                    'lastName' => $item->user["lastName"]
                ],
                'profile' => [
                    'phone' => $item->profile["phone"],
                    'society' => $item->profile["society"],
                    'profile' => $item->profile["profile"],
                    'ref' => $item->profile["professionalRef"]
                ]
            ];
        }, 
        $list);
    }

}
