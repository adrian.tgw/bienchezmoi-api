<?php
//namespace App;
namespace Models;

use Phalcon\Mvc\MongoCollection;
use Phalcon\Db\Adapter\MongoDB\Operation;

class Log extends MongoCollection
{
    public $uid = "";
    public $status = 0;

    public function getSource()
    {
        return 'log';
    }

    public function exists($uid)
    {
        $filter = 
        [
            'conditions' => 
            [
                'uid' => new \MongoDB\BSON\ObjectId($uid), 
                'status' => 1 
            ]
        ];

        $exists = $this->findFirst($filter);

        return (bool) $exists;
    }

    public function existsAnonym($user)
    {
        $filter = 
        [
            'conditions' => 
            [
                'uid' => $user, 
                'status' => 1 
            ]
        ];

        $exists = $this->findFirst($filter);

        return (bool) $exists;
    }

    public function getByUid($uid)
    {
        $filter = ['conditions' => ['uid' => new \MongoDB\BSON\ObjectId($uid) ]];
        return $this->findFirst($filter);
    }

    public function registerNew($uid)
    {
        $self = new Log();
        $self->uid = new \MongoDB\BSON\ObjectId($uid);
        $self->created_at = date("Y-m-d h:i:s");
        $self->status = 1;

        if ( $self->save() )
            return $self;
            
        return false;
    }

    public function registerNewAnonym($user)
    {
        $self = new Log();
        $self->uid = $user;
        $self->created_at = date("Y-m-d h:i:s");
        $self->status = 1;

        if ( $self->save() )
            return $self;
            
        return false;
    }

    public function getUid()
    {
        return (string) $this->uid;
    }

    public function getAvailable()
    {
        $filter = [
            'conditions' => [
                'status' =>  1
            ]
        ];

        $results = $this->find($filter);
        if (!$results) {
            return array();
        }

        return $results;
    }


}
