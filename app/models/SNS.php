<?php
//namespace App;
namespace Models;

use Phalcon\Mvc\MongoCollection;
use Phalcon\Db\Adapter\MongoDB\Operation;

class SNS extends MongoCollection
{
    public $uid;
    public $device_token;
    public $topics;

    public function getSource()
    {
        return 'sns';
    }

    public function exists($input)
    {
        $filter = ['conditions' => []];

        if ( isset($input['device_token']) )
            $filter['conditions']['device_token'] = $input['device_token'];

        if ( isset($input['uid']) )
            $filter['conditions']['uid'] = $input['uid'];

        if ( isset($input['subject']) )
            $filter['conditions']['subject'] = $input['subject'];

        if ( isset($input['created_at']) )
            $filter['conditions']['created_at'] = $input['created_at'];

        $exists = $this->findFirst($filter);

        return (bool) $exists;
    }

}
