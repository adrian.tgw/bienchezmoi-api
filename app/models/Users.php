<?php namespace Models;

use Phalcon\Mvc\MongoCollection;
use Phalcon\Db\Adapter\MongoDB\Operation;

use ParagonIE\Halite\HiddenString;
use ParagonIE\Halite\Password;

class Users extends MongoCollection
{
    public $name;
    public $lastName;
    public $email;
    public $status;
    public $registrationType;
    public $permissions;
    
    public $notification;
    public $devices;
    public $topics;
    public $push_messages;
    public $badges;

    public $version;
    public $app_version;
    public $lang;

    public $password;
    public $socialPassword;

    public $socialUser;

    public function getSource()
    {
        return 'users';
    }

    public function initialize()
    {
        $this->name = "";
        $this->lastName = "";
        $this->email = "";
        $this->password = "";
        $this->socialPassword = "";
        $this->socialUser = "";
        $this->permissions = 0;
        $this->registrationType = 0;
        $this->notification = false;
        $this->version = 0.0;
        $this->app_version = 0.0;
        $this->devices = array();
        $this->topics = array();
        $this->push_messages = array();
        $this->badges = 0;

        $this->lang = "fr";
        $this->status = 0;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($passwd)
    {
        $this->password = $passwd;
    }

    public function setPasswordSocial($passwd)
    {
        $this->passwordSocial = $passwd;
    }

    public function exists($input)
    {
        $filter = ['conditions' => []];

        if ( !isset($input['email']) )
            return false;
        
        $filter['conditions']['email'] = $input['email'];
        $exists = $this->findFirst($filter);

        return (bool) $exists;
    }

    public function newUser($user)
    {
        if ($this->existsLocal($user['mail'])) {
            return true;
        }

        return $this->registerNew($user);
    }

    public function setUserConfirmed($self)
    {
        $self->status = 1;
        $self->permissions = 1;
        $self->updated_at = date("Y-m-d h:i:s");

        if ( $self->save() )
            return $self;
            
        return false;
    }

    public function setUserActive($self)
    {
        $self->status = 1;
        $self->permissions = 1;
        $self->updated_at = date("Y-m-d h:i:s");

        if ( $self->save() )
            return $self;
            
        return false;
    }

    public function setUserDeactive($self)
    {
        $self->status = 0;
        $self->permissions = 0;
        $self->updated_at = date("Y-m-d h:i:s");

        if ( $self->save() )
            return $self;
            
        return false;
    }

    public function setUserInfo($self, $input)
    {
        $self->name = $input["name"];
        $self->lastName = $input["lastName"];
        $self->updated_at = date("Y-m-d h:i:s");

        if ( $self->save() )
            return $self;
            
        return false;
    }

    public function updateUserPassword($passwd, $secretKey)
    {
        $password = new HiddenString($passwd);
        $hash = Password::hash($password, $secretKey);
        $this->setPassword( $hash );
        return $this->updateUser($this);
    }

    public function getLocalUserById($id)
    {
        $userFound = Users::findById($id);
        return $userFound;
    }

    public function getLocalUser($email)
    {
        $filter = ['conditions' => ['email' => $email ]];
        $userFound = $this->findFirst($filter);
        return $userFound;
    }

    public function getLocalUserSocial($user)
    {
        $filter = ['conditions' => ['socialUser' => $user ]];
        $userFound = $this->findFirst($filter);
        return $userFound;
    }

    public function existsLocal($email)
    {
        $filter = ['conditions' => []];
        $filter['conditions']['email'] = $email;

        $exists = $this->findFirst($filter);

        return (bool) $exists;
    }

    public function existsWithToken($token)
    {
        $filter = ['conditions' => []];
        $filter['conditions']['remember_token'] = $token;

        $exists = $this->findFirst($filter);

        return $exists;
    }

    public function findWithEmail($email)
    {
        $filter = ['conditions' => []];
        $filter['conditions']['email'] = $email;

        $user = $this->findFirst($filter);

        return $user;
    }

    public function registerNew($input)
    { 
        $self = new Users();
        $self->initialize();
        $self = $this->setNewUser( $self, $input );

        if ( $self->save() ) 
            return $self;
            
        return false;
    }

    public function registerNewWithPassword($input, $secretKey)
    {
        $self = new Users();
        $self->initialize();
        $self = $this->setNewUser( $self, $input );

        $password = new HiddenString($input['password']);
        $hash = Password::hash($password, $secretKey);
        $self->setPassword( $hash );

        if ( $self->save() )
            return $self;

        return false;
    }

    public function registerNewWithPasswordSocial($input, $secretKey)
    {
        $self = new Users();
        $self->initialize();
        $self = $this->setNewUser( $self, $input );

        $password = new HiddenString($input['password']);
        $hash = Password::hash($password, $secretKey);
        $self->setPassword( $hash );
        $self->setPasswordSocial( $hash );

        if ( $self->save() )
            return $self;

        return false;
    }

    public function registerNewUserApple($email, $user, $name, $lastName)
    {
        $self = new Users();
        $self->initialize();
        $self = $this->setNewUser( $self, [
            "name" => $name,
            "lastName" => $lastName,
            "email" => $email,
            "type" => 4,
            "withNotifications" => true
        ] );

        $self->version = 2.0;
        $self->socialUser = $user;
        $self->status = 1;
        $self->permissions = 1;

        if ( $self->save() )
            return $self;

        return false;
    }

    private function setNewUser( $self, $input )
    {
        $self->name = $input["name"];
        $self->lastName = $input["lastName"];
        $self->email = $input["email"];
        $self->registrationType = intval( $input["type"] );
        $self->notification = true;
        if ( isset($input["withNotifications"]) ) {
            $self->notification = intval($input["withNotifications"]) === 1 ? true : false;
        }
        $self->status = 0;
        $self->permissions = 0;
        $self->version = 1.0;
        $self->app_version = 1.0;

        $self->push_messages = array();
        $self->devices = [ ["token" => "notoken"] ];
        $self->topics = ["general", "noflashes"];
        $self->created_at = date("Y-m-d h:i:s");
        return $self;
    }

    public function existsSocialUser($user, $returnBool=true)
    {
        $filter = ['conditions' => ['socialUser' => $user]];

        $exists = $this->findFirst($filter);

        if ( $returnBool )
            return (bool) $exists;
        else
            return $exists;
    }

    public function loginExists($var, $returnBool=true)
    {
        $filter = ['conditions' => ['login' => $var]];

        $exists = $this->findFirst($filter);

        if ( $returnBool )
            return (bool) $exists;
        else
            return $exists;
    }

    public function getAllUsers()
    {
        $filter = [ 'conditions' => [] ];
        $list = $this->find($filter);
        $data = [];
        foreach ( $list as $user ) {
            $data[] = $this->modelFormat($user);
        }
        return $data;
    }

    public function updateUser($self)
    {
        $self->updated_at = date("Y-m-d h:i:s");

        if ( $self->save() )
            return $self;
            
        return false;
    }

    // Model Value Interaction
    public function modelFormat($user)
    {
        return [
            'id' => (string) $user->_id,
            'name' => $user->name,
            'lastName' => $user->lastName,
            'email' => $user->email,
            'registrationType' => $user->registrationType,
            'registrationTitle' => $this->getRegistrationName($user),
            'permissions' => $user->permissions,
            'notification' => $user->notification,
            'badges' => @$user->badges,
            'profile' => @$user->profile,
            'devices' => @$user->devices,
            'push_messages' => @$user->push_messages,
            'status' => $user->status,
            'created' => @$user->created_at
        ];
    }

    private function getRegistrationName($user)
    {
        if ($user->registrationType === 1) {
            return "Normal";
        }

        if ($user->registrationType === 2) {
            return "Google";
        }

        if ($user->registrationType === 3) {
            return "Facebook";
        }

        if ($user->registrationType === 4) {
            return "Apple";
        }

        return " - ";
    }

    public function modelFormatOnList($list)
    {
        return array_map(
            function($item)
        {
            return $this->modelFormatProfile($item);
        },
        $list);
    }

    public function modelFormatProfile($user)
    {
        return [
            'id' => (string) $user->_id,
            'name' => $user->name,
            'lastName' => $user->lastName,
            'email' => $user->email,
            'registrationType' => $user->registrationType,
            'registrationTitle' => $this->getRegistrationName($user),
            'permissions' => $user->permissions,
            'notification' => $user->notification,
            'badges' => @$user->badges,
            'push_messages' => @$user->push_messages,
            'socialUser' => @$user->socialUser,
            'devices' => @$user->devices,
            'topics' => @$user->topics,
            'created' => @$user->created_at,
            'status' => $user->status,
            'profile' => $user->profile,
            'version' => $user->version,
            'app_version' => 1.0
        ];
    }
    
    public function modelFormatMajor($user)
    {
        return [
            'id' => (string) $user->_id,
            'name' => $user->name,
            'lastName' => $user->lastName,
            'email' => $user->email,
            'registrationType' => $user->registrationType,
            'registrationTitle' => $this->getRegistrationName($user),
            'permissions' => $user->permissions,
            'notification' => $user->notification,
            'socialUser' => @$user->socialUser,
            'badges' => @$user->badges,
            'push_messages' => @$user->push_messages,
            'devices' => @$user->devices,
            'topics' => @$user->topics,
            'created' => @$user->created_at,
            'status' => $user->status,
            'version' => $user->version,
            'app_version' => 1.0
        ];
    }

}
