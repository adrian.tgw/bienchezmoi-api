<?php

namespace Models;

class FlashMessage
{
    public $title;
    public $body;
    public $idReference;
    public $flashID;

    function __construct($title, $body, $idReference) 
    {
        $this->title = $title;
        $this->body = $body;
        $this->idReference = $idReference;

        $this->flashID = md5($title);
    }
}
