<?php namespace Models;

use Phalcon\Mvc\MongoCollection;
use Phalcon\Db\Adapter\MongoDB\Operation;

class FlashNotification extends MongoCollection
{
    public $item_id;
    public $title;
    public $body;
    public $short;
    public $images;
    public $date_sent;
    public $send;
    public $totalSent;
    public $success;
    public $failed;
    public $profile = 0;
    public $status;
    public $created_at;

    public function initialize() 
    {
        $this->title = "";
        $this->body = "";
        $this->short = "";
        $this->images = array();
        $this->date_sent = "";
        $this->send = false;
        $this->totalSent = 0;
        $this->success = 0;
        $this->failed = 0;
        $this->profile = 0;
        $this->status = 0;
    }

    // DB Interaction
    public function existsId($id)
    {
        $filter = ['conditions' => []];
        $filter['conditions']['id'] = $id;

        $exists = $this->findFirst($filter);

        return (bool) $exists;
    }

    // DB Interaction
    public function getFlashNotificationById($id)
    {
        try 
        {
            $self = FlashNotification::findById($id);
                return $self;
            
            return false;
        } catch ( \Exception $e ) {
            return false;
        }
    }

    // DB Interaction
    public function getAllFlashNotifications() 
    {
        $filter = ['conditions' => []];

        try
        {
            $self = FlashNotification::find($filter);
            return $self;
        } catch ( \Exception $e ) {
            return false;
        }
    }

    // DB Interaction
    public function getAvailableFlashNotifications() 
    {
        $filter = ['conditions' => []];
        $filter['conditions']['status'] = 1;

        try
        {
            $self = FlashNotification::find($filter);
            return $self;
        } catch ( \Exception $e ) {
            return false;
        }
    }


    // DB Interaction
    public function getSentFlashNotifications( ) 
    {
        $filter = ['conditions' => []];
        $filter['conditions']['status'] = 1;
        $filter['conditions']['send'] = true;

        try
        {
            $self = FlashNotification::find($filter);
            $list = [];
            foreach($self as $item) {

                if ($item->profile === null || $item->profile === 0) {
                    $list[] = $item;
                }

            }
            return $list;
        } catch ( \Exception $e ) {
            return false;
        }
    }

    public function newFlashNotification($input)
    {
        $flashNotification = new FlashNotification();
        $flashNotification->initialize();

        try {
            $flashNotification = $this->setValues($flashNotification, $input);
            $flashNotification->status = 1;

            return $flashNotification;
        } catch ( \Exception $e ) {
            return false;
        }
    }

    public function updateFlashNotification($input)
    {
        try {
            $flashNotification = $this->setValues($this, $input);
            $flashNotification->status = intval($input["status"]);

            return $flashNotification;
        } catch ( \Exception $e ) {
            return false;
        }
    }

    private function setValues($self, $input)
    {
        $self->title =  $input["title"];
        $self->body = $input["body"];
        $self->short = $input["short"];
        $self->profile = intval($input["profile"]);

        return $self;
    }

    public function updateFlashData($response)
    {
        $this->send = true;
        $this->date_sent = date("Y-m-d h:i:s");
        $this->totalSent = $response["total"];
        $this->success = $response["success"];
        $this->failed = $response["error"];   
    }

    public function setImages( $images )
    {
        $this->images = $images;
    }

    // DB Interaction
    public function register()
    {
        if ($this->status == 0) return false;
        $this->created_at = date("Y-m-d h:i:s");
        
        if ( $this->save() ) 
            return $this;
        else 
            return false;
    }

    // DB Interaction
    public function update()
    {
        $this->updated_at = date("Y-m-d h:i:s");
        if ( $this->save() ) 
            return $this;
        else 
            return false;
    }

    // DB Interaction
    public function deleteNotification()
    {
        if ( $this->delete() ) 
            return true;
        else 
            return false;
    }

    // Model Value Interaction
    public function modelFormatOnList($list)
    {
        return array_map(
            function($item)
        {
            return $this->modelFormat($item);
        }, 
        $list);
    }

    // Model Value Interaction
    public function modelFormat($item)
    {
        return [
            'id' => (string) $item->getId(),
            'title' => $item->title,
            'body' => $item->body,
            'short' => $item->short,
            'images' => $item->images,
            'profile' => $item->profile,
            'dateSent' => $item->date_sent,
            'send' => $item->send,
            'totalSent' => $item->totalSent,
            'success' => $item->success,
            'failed' => $item->failed,
            'status' => $item->status
        ];
    }
}
