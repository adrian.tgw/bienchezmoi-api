<?php namespace Models;

use Phalcon\Mvc\MongoCollection;
use Phalcon\Db\Adapter\MongoDB\Operation;

class UserRecovery extends MongoCollection
{
    public $uid;
    public $status;

    public function getSource()
    {
        return 'user_recovery';
    }

    public function newToken($uid)
    {
        $this->status = 1;
        $this->uid = $uid;
        $this->created_at = date("Y-m-d H:i:s");
        if ( $this->save() )
            return true;
            
        return false;
    }

    public function update()
    {
        $this->status = 0;
        if ( $this->save() ) 
            return $this;
        
        return false;
    }

    public function exists($id)
    {
        $found = UserRecovery::findById($id);
        return (bool) $found ? $found : false;
    }

    public function getToken($id) 
    {
        $tokenFound = $this->exists($id);
        if ($tokenFound) {
            return $tokenFound;
        }
        
        return false;
    }
}
