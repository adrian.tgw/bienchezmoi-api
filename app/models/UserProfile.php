<?php 
declare(strict_types=1);
namespace Models;

use Phalcon\Mvc\MongoCollection;
use Phalcon\Db\Adapter\MongoDB\Operation;

class UserProfile extends MongoCollection
{
    const INDIVIDUAL = 1;
    const PROFESSIONAL = 2;

    public $uid;
    public $profile; // professional == 2 or individual == 1
    public $district;
    public $municipality;
    public $city;
    public $phone;
    public $announces;

    public $society;
    public $professionalRef;

    public $status;

    public function getSource()
    {
        return 'user_profile';
    }

    public function initialize()
    {
        $this->uid = "";

        $this->profile = 0;
        $this->district = "";
        $this->municipality = "";
        $this->city = "";
        $this->phone = "";
        $this->announces = array();
        
        $this->society = "";
        $this->professionalRef = "";

        $this->status = 0;
    }

    public function existsLocal($uid)
    {
        $filter = ['conditions' => []];
        $filter['conditions']['uid'] = new \MongoDB\BSON\ObjectId($uid);

        $exists = $this->findFirst($filter);

        return (bool) $exists;
    }
    
    public function registerNewEmpty($uid)
    {
        $self = new UserProfile();
        $self->initialize();
        $self = $this->registerUserProfile( $self, $uid );

        if ( $self->save() ) 
            return $self;
            
        return false;
    }
    
    public function registerNew($uid, $input)
    {
        $self = new UserProfile();
        $self->initialize();
        $self = $this->newUser( $self, $uid, $input );

        if ( $self->save() )
            return $self;
            
        return false;
    }

    private function newUser( $self, $uid, $input )
    {
        $self = $this->registerUserProfile( $self, $uid );
        return $this->setUserValues( $self, $input );
    }

    private function registerUserProfile( $self, $uid )
    {
        $self->uid = new \MongoDB\BSON\ObjectId($uid);
        $self->created_at = date("Y-m-d h:i:s");
        return $self;
    }

    public function registerUserValues($uid, $input)
    {
        $self = $this->getByUid($uid);
        $self = $this->setUserValues( $self, $input );

        if ( $self->save() )
            return $self;
            
        return false;
    }

    public function setUserValues( $self, $input )
    {
        $self->profile = intval( $input["profile"] );

        $self->district = $input["district"];
        $self->municipality = $input["municipality"];
        $self->city = $input["city"];
        $self->phone = $input["phone"];

        $self->society = isset ( $input["society"] ) ? $input["society"] : "";
        $self->professionalRef = isset ( $input["professionalRef"] ) ? $input["professionalRef"] : "";

        $self->status = 1;

        return $self;
    }

    public function updateValues($uid, $input)
    {
        $self = $this->getByUid($uid);

        $self->district = $input["district"];
        $self->municipality = $input["municipality"];
        $self->city = $input["city"];
        $self->phone = $input["phone"];
        
        if ( $self->save() )
            return $self;
            
        return false;
    }

    public function updateUserProfile($self)
    {
        $self->updated_at = date("Y-m-d h:i:s");

        if ( $self->save() )
            return $self;
            
        return false;
    }

    public function addAnnounce($id)
    {
        $this->announces[] = $id;
    }

    public function removeAnnounce($id)
    {
        $list = [];
        foreach($this->announces as $item)
        {
            if ((string) $item !== $id) {
                $list[] = $item;
            }
        }
        $this->announces = $list;
    }

    public function getById($id)
    {
        return UserProfile::findById($id);
    }

    public function getByUid($uid)
    {
        $filter = ['conditions' => ['uid' => new \MongoDB\BSON\ObjectId($uid) ]];
        return $this->findFirst($filter);
    }

    
    // Model Value Interaction
    public function modelFormat($user)
    {
        return [
            'id' => (string) $user->_id,
            'uid' => (string) $user->uid,
            'profile' => $user->profile,
            'district' => $user->district,
            'municipality' => $user->municipality,
            'city' => $user->city,
            'phone' => $user->phone,
            'announces' => @$user->announces,
            'society' => @$user->society,
            'professionalRef' => @$user->professionalRef,
            'status' => $user->status
        ];
    }

    /*
    public function modelFormatMajor($user)
    {
        return [
            'id' => (string) $user->_id,
            'name' => $user->name,
            'lastName' => $user->lastName,
            'email' => $user->email,
            'permissions' => $user->permissions,
            'notification' => $user->notification,
            'badges' => @$user->badges,
            'push_messages' => @$user->push_messages,
            'devices' => @$user->devices,
            'topics' => @$user->topics,
            'created' => @$user->created_at,
            'status' => $user->status,
            'version' => 1.0,
            'app_version' => 1.0
        ];
    }
    */

}
