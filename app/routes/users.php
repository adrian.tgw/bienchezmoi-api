<?php
use Phalcon\Mvc\Router\Group as RouterGroup;

// Create a group with a common module and controller
$module = new RouterGroup([ "controller" => "users" ]);
$module->setPrefix("/users");

$module->add("/get-all", [ "action" => "getAll" ])->via(['GET']);
$module->add("/get-all-with-profile", [ "action" => "getAllProfile" ])->via(['GET']);
$module->add("/verify-user/{email}", [ "action" => "verifyUser" ])->via(['GET']);
$module->add("/get-user/{email}", [ "action" => "getUser" ])->via(['GET']);
$module->add("/get-user-profile/{email}", [ "action" => "getUserProfile" ])->via(['GET']);
$module->add("/me", [ "action" => "getMe" ])->via(['GET']);
$module->add("/new", [ "action" => "new" ])->via(['PUT', 'POST']);
$module->add("/new-social", [ "action" => "newSocial" ])->via(['PUT', 'POST']);
$module->add("/new-professional", [ "action" => "newProfessional" ])->via(['PUT', 'POST']);
$module->add("/set-profile", [ "action" => "setProfile" ])->via(['PUT', 'POST']);
$module->add("/update-status", [ "action" => "updateStatus" ])->via(['PUT', 'POST']);
$module->add("/update-password", [ "action" => "updatePassword" ])->via(['PUT', 'POST']);
$module->add("/update-info", [ "action" => "updateInfo" ])->via(['PUT', 'POST']);
$module->add("/update-profile", [ "action" => "updateProfile" ])->via(['PUT', 'POST']);
//$module->add("/admin", [ "action" => "newAdmin" ])->via(['PUT', 'POST']);
$module->add("/confirmation-email/{id:[0-9a-z]+}", [ "action" => "confirmationEmail" ])->via(['PUT', 'POST']);
$module->add("/delete/{id}", [ "action" => "deleteUser" ])->via(['PUT', 'POST']);
$module->add("/password-recovery/{email}", [ "action" => "passwordrecovery" ])->via(['GET', 'POST']);
$module->add("/password-recovery-update/{id:[0-9a-z]+}", [ "action" => "passwordrecoveryupdate" ])->via(['POST']);
$module->add("/app-version-set", [ "action" => "appVersionSet" ])->via(['POST']);
$module->add("/settings", [ "action" => "settings" ])->via(['POST']);
$router->mount($module);
