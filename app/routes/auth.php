<?php
use Phalcon\Mvc\Router\Group as RouterGroup;

$module = new RouterGroup([ "controller" => "auth" ]);
$module->setPrefix("/auth");
$module->add("/", [ "action" => "index" ])->via(['GET']);
$module->add("/login", [ "action" => "login" ])->via(['POST']);
$module->add("/login-social-apple", [ "action" => "loginSocialApple" ])->via(['POST']);
$module->add("/anonymous", [ "action" => "anonymous" ])->via(['POST']);
$module->add("/status-token", [ "action" => "statusToken" ])->via(['GET']);
$router->mount($module);
