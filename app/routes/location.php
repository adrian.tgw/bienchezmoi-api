<?php
use Phalcon\Mvc\Router\Group as RouterGroup;

// Create a group with a common module and controller
$module = new RouterGroup([ "controller" => "location" ]);
$module->setPrefix("/location");
$module->add("/new", [ "action" => "new" ])->via(['PUT', 'POST']);
$module->add("/update/{id:[0-9A-Za-z_\s]+$}", [ "action" => "update" ])->via(['PUT', 'POST']);
$module->add("/read-from-file/{filename:[0-9A-Za-z_\s]+$}", [ "action" => "readFromFile" ])->via(['PUT', 'POST']);
$module->add("/get/{id:[0-9A-Za-z_\s]+$}", [ "action" => "get" ])->via(['GET']);
$module->add("/get-all", [ "action" => "getAll" ])->via(['GET']);
$module->add("/get-available", [ "action" => "getAvailable" ])->via(['GET']);
$module->add("/get-all-by-type/{type}", [ "action" => "getByType" ])->via(['GET']);
$module->add("/get-available-by-type/{type}", [ "action" => "getAvailableByType" ])->via(['GET']);
$module->add("/get-all-by-parent/{id:[0-9A-Za-z_\s]+$}", [ "action" => "getByParent" ])->via(['GET']);
$module->add("/get-available-by-parent/{id:[0-9A-Za-z_\s]+$}", [ "action" => "getAvailableByParent" ])->via(['GET']);
$module->add("/delete/{id:[0-9A-Za-z_\s]+$}", [ "action" => "delete" ])->via(['PUT', 'POST']);
$router->mount($module);
