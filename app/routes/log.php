<?php
use Phalcon\Mvc\Router\Group as RouterGroup;

$module = new RouterGroup([ "controller" => "log" ]);
$module->setPrefix("/log");
$module->add("/new-events", [ "action" => "newEvents" ])->via(['POST']);
$module->add("/new-events-anonym", [ "action" => "newEventsAnonym" ])->via(['POST']);
$module->add("/get-events/{count}/{type}", [ "action" => "getEvents" ])->via(['GET']);
$module->add("/download-events/{token}/{count}/{type}", [ "action" => "downloadEvents" ])->via(['GET']);
$router->mount($module);
