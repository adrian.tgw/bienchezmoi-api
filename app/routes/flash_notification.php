<?php
use Phalcon\Mvc\Router\Group as RouterGroup;

// Create a group with a common module and controller
$module = new RouterGroup([ "controller" => "flashnotification" ]);
$module->setPrefix("/flash_notification");
$module->add("/get/{id:[0-9A-Za-z_\s]+$}", [ "action" => "getFlashNotification" ])->via(['GET']);
$module->add("/get-all-flash-notifications", [ "action" => "getAllFlashNotifications" ])->via(['GET']);
$module->add("/get-sent-notifications", [ "action" => "getSentNotifications" ])->via(['GET']);
$module->add("/get-available-flash-notifications", [ "action" => "getAvailableFlashNotifications" ])->via(['GET']);
$module->add("/send/{id:[0-9A-Za-z_\s]+$}", [ "action" => "sendPushNotification" ])->via(['PUT', 'POST']);
$module->add("/new", [ "action" => "newNotification" ])->via(['PUT', 'POST']);
$module->add("/update/{id:[0-9A-Za-z_\s]+$}", [ "action" => "updateNotification" ])->via(['PUT', 'POST']);
$module->add("/delete/{id:[0-9A-Za-z_\s]+$}", [ "action" => "deleteNotification" ])->via(['PUT', 'POST']);

$router->mount($module);
