<?php
use Phalcon\Mvc\Router\Group as RouterGroup;

// Create a group with a common module and controller
$module = new RouterGroup([ "controller" => "sns" ]);
$module->setPrefix("/sns");
$module->add("", [ "action" => "index" ])->via(['GET']);
$module->add("/topics", [ "action" => "topics" ])->via(['GET']);
$module->add("/applications", [ "action" => "applications" ])->via(['GET']);
$module->add("/is-device-registered/{id:[a-zA-Z0-9\_\-]+}", [ "action" => "isDeviceRegistered" ])->via(['GET']);
$module->add("/user-push-notifications", [ "action" => "userPushNotifications" ])->via(['GET']);
$module->add("/user-push-devices/{topic:[0-9a-z]+}", [ "action" => "userPushDevices" ])->via(['GET']);
$module->add("/register-user-device", [ "action" => "registerNewDevice" ])->via(['POST']);
$module->add("/remove-user-device", [ "action" => "removeUserDevice" ])->via(['POST']);
$module->add("/send-test-notification", [ "action" => "sendTestNotification" ])->via(['POST']);
$module->add("/send-tests-notifications", [ "action" => "sendTestsTopicNotifications" ])->via(['POST']);
$module->add("/reset-badges-count", [ "action" => "resetBadgesCount" ])->via(['POST']);
$module->add("/reset-users-push", [ "action" => "resetUsersPush" ])->via(['POST']);
$module->add("/multiple-devices", [ "action" => "multipleDevices" ])->via(['GET']);
$module->add("/update-user-push-notifications", [ "action" => "updateUserPushNotifications" ])->via(['POST']);
// $module->add("/add-user-device/{id:[0-9a-z]+}", [ "action" => "addDeviceToApplication" ])->via(['POST']);
$module->add("/publish/{id:[0-9a-z]+}", [ "action" => "publish" ])->via(['POST']);
$router->mount($module);
