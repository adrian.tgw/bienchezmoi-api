<?php
use Phalcon\Mvc\Router\Group as RouterGroup;

// Create a group with a common module and controller
$module = new RouterGroup([ "controller" => "publish" ]);
$module->setPrefix("/publish");
$module->add("/get/{id}", [ "action" => "get" ])->via(['GET']);
$module->add("/get-available", [ "action" => "getAvailable" ])->via(['GET']);
$module->add("/get-by-search", [ "action" => "getBySearch" ])->via(['GET']);
$module->add("/get-all", [ "action" => "getAll" ])->via(['GET']);
$module->add("/get-user", [ "action" => "getUser" ])->via(['GET']);
$module->add("/new", [ "action" => "new" ])->via(['POST', 'PUT']);
$module->add("/update", [ "action" => "update" ])->via(['POST', 'PUT']);
$module->add("/update-status", [ "action" => "updateStatus" ])->via(['POST', 'PUT']);
$module->add("/activate", [ "action" => "activate" ])->via(['POST', 'PUT']);
$module->add("/remove", [ "action" => "remove" ])->via(['POST', 'PUT']);
$module->add("/delete/{id}", [ "action" => "delete" ])->via(['POST', 'PUT']);

$router->mount($module);
