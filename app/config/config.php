<?php

/*
 * Modified: prepend directory path of current file, because of this file own different ENV under between Apache and command line.
 * NOTE: please remove this comment.
 */
defined('BASE_PATH') || define('BASE_PATH', getenv('BASE_PATH') ?: realpath(dirname(__FILE__) . '/../..'));
defined('APP_PATH') || define('APP_PATH', BASE_PATH . '/app');

require_once APP_PATH . '/helpers/base.php';

return new \Phalcon\Config([
    'email' => [
        'replyTo' => 'support@appbienchezmoi.com'
    ],
    'aws' => [
        'key' => 'AKIAIWMRFHW57WCGKLTA',
        'secret' => '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',
        'region' => 'eu-central-1',
        'version' => 'latest'
    ],
    'database' => [
        'adapter'     => 'MongoDB',
        'host'        => 'localhost',
        'username'    => 'root',
        'password'    => '',
        // 'dbname'      => 'podcastimmotest',
        //'dbname'      => 'bienchezmoidev',
        //'dbname'      => 'bienchezmoistaging',
        'dbname'      => 'bienchezmoiprod',
        'charset'     => 'utf8',
    ],
    'application' => [
        'url'            => '',
        'name'           => "BienChezMoi App",
        'appDir'         => APP_PATH . '/',
        'controllersDir' => APP_PATH . '/controllers/',
        'modelsDir'      => APP_PATH . '/models/',
        'incubatorDir'      => APP_PATH . '/vendor/phalcon/',
        'tokenDir'      => APP_PATH . '/token/',
        'routesDir'      => APP_PATH . '/routes/',
        'viewsDir'       => APP_PATH . '/views/',
        'servicesDir'    => APP_PATH . '/services/',
        'presentersDir'    => APP_PATH . '/presenters/',
        'pluginsDir'     => APP_PATH . '/plugins/',
        'libraryDir'     => APP_PATH . '/library/',
        'cacheDir'       => BASE_PATH . '/cache/',
        'baseUri'        => '/',
    ]
]);
