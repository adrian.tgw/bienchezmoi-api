<?php

$router = $di->getRouter(false);
$router->removeExtraSlashes(true);
$router->setDefaultNamespace('Controllers');

//include_once APP_PATH . '/routes/default.php';

// Import routes form app/routes dir
$routesList = scandir(APP_PATH . '/routes');
$curRoute   = currentNode();

if ( $curRoute!='.' && $curRoute!='..' && file_exists(APP_PATH . '/routes/'. $curRoute.'.php') ) 
{
    include_once APP_PATH . '/routes/' . $curRoute.'.php';
}

$router->handle();