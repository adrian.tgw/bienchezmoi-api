<?php

$loader = new \Phalcon\Loader();

$loader->registerNamespaces(
    [
       "Controllers" => $config->application->controllersDir,
       "Models"      => $config->application->modelsDir,
       "Routes"      => $config->application->routesDir,
       "Services"    => $config->application->servicesDir,
       "Presenters"    => $config->application->presentersDir,
       "Phalcon" => $config->application->incubatorDir,
       "Token" => $config->application->tokenDir,
    ]
);

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerDirs(
    [
        $config->application->controllersDir,
        $config->application->modelsDir,
        $config->application->routesDir
    ]
)->register();
