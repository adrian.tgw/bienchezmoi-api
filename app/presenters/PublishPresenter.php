<?php 
declare(strict_types=1);
namespace Presenters;

use Phalcon\Validation\Validator\MongoId;

use Models\Users;
use Models\UserProfile;
use Models\Publish; 

use Services\Email;

use Services\SaveImageService;

class PublishPresenter
{
    public function __construct($config)
    { 
        $this->config = $config;
    }

    public function getPublish($id)
    {
        try
        {
            //$user = $usersModel->getLocalUserById($id);
            //if (!$user) return ["status" => 2, "message" => "User not found !"];
            
            $publishModel = new Publish();
            $item = $publishModel->getById($id);
            
            if (!$item) return ["status" => 2, "message" => "Item not found !"];
            
            
            $userProfileModel = New UserProfile();
            $userProfile = $userProfileModel->getByUid( (string) $item->uid );
            
            if (!$userProfile) return ["status" => 2, "message" => "User Profile not found !"];
            
            $usersModel = new Users();
            $user = $usersModel->getLocalUserById( (string) $item->uid );
            if (!$user) return ["status" => 2, "message" => "User not found !"];
            
            $user->profile = $userProfile;
            $user->id = (string) $user->_id;
            $item->user = $user;
            $item->id = (string) $item->_id;

            return ["status" => 1, "message" => "get success !", "result" => $item ];   

        } catch ( \Exception $e ) {
            return ["status" => 0, "message" => "Server error !"];
        }
    }

    public function getUserPublish($uid)
    {
        $usersModel = New Users();
        $user = null;
        try
        {
            //$user = $usersModel->getLocalUserById($uid);
            //if (!$user) return ["status" => 2, "message" => "User not found !"];

            $userProfileModel = New UserProfile();
            $userProfile = $userProfileModel->getByUid( $uid );
            
            if (!$userProfile) return ["status" => 2, "message" => "User Profile not found !"];

            $publishModel = new Publish();
            $items = $publishModel->getAllByUid($uid);
            return ["status" => 1, "message" => "success !", "result" => $publishModel->modelFormatOnList( $items ) ];   

        } catch ( \Exception $e ) {
            return ["status" => 0, "message" => "Server error !"];
        }
    }

    public function newUserPublish($uid, $input)
    {
        $usersModel = New Users();
        $user = null;
        try
        {
            $user = $usersModel->getLocalUserById( $uid);
            if (!$user) return ["status" => 2, "message" => "User not found !"];

            $userProfileModel = New UserProfile();
            $userProfile = $userProfileModel->getByUid( $uid );
            
            if (!$userProfile) return ["status" => 2, "message" => "User Profile not found !"];
            
            if ( count($userProfile->announces) >= 3 && $userProfile->profile == UserProfile::INDIVIDUAL ) 
            {
                return ["status" => 5, "message" => "Publish Limit !"];
            }

            $publishModel = new Publish();

            if ($userProfile->profile == UserProfile::INDIVIDUAL) {
                $itemSaved = $publishModel->registerNew( $uid, $input );
            } else {
                $itemSaved = $publishModel->registerNewForProfessional( $uid, $input );
            }

            if (!$itemSaved) return ["status" => 3, "message" => "There was a problem while doing the update !"];

            $userProfile->addAnnounce( $itemSaved->getId() );
            $userProfileModel->updateUserProfile($userProfile);

            $saveImage = new SaveImageService();
            $itemId = (string) $itemSaved->getId();
            $imagesNames = $saveImage->savePublishPhotos($input, $itemId);
            $itemSaved->setPhotos( $imagesNames );
            $itemSaved = $itemSaved->update();

            if ($userProfile->profile == UserProfile::INDIVIDUAL) {
                $usersModel = New Users();
                $user = $usersModel->getLocalUserById( $uid );
                $this->sendPublishToConfirmEmail($user, $itemSaved);
            }
            
            return ["status" => 1, "message" => "updated success !", "result" => $publishModel->modelFormat( $itemSaved ) ];   
            

        } catch ( \Exception $e ) {
            return ["status" => 0, "message" => "Server error !"];
        }
    }

    public function updateUserPublish($uid, $input)
    {
        $usersModel = New Users();
        $user = null;
        try
        {
            //$user = $usersModel->getLocalUserById($id);
            //if (!$user) return ["status" => 2, "message" => "User not found !"];

            $userProfileModel = New UserProfile();
            $userProfile = $userProfileModel->getByUid( $uid );
            
            if (!$userProfile) return ["status" => 2, "message" => "User Profile not found !"];

            $publishModel = new Publish();
            $itemSaved = $publishModel->updatePublish( $input["id"], $input );

            if (!$itemSaved) return ["status" => 3, "message" => "There was a problem while doing the update !"];

            $saveImage = new SaveImageService();
            $itemId = (string) $itemSaved->getId();
            $imagesNames = $saveImage->savePublishPhotos($input, $itemId);
            $itemSaved->setPhotos( $imagesNames );
            $itemSaved = $itemSaved->update();
            
            return ["status" => 1, "message" => "updated success !", "result" => $publishModel->modelFormat( $itemSaved ) ];   

        } catch ( \Exception $e ) {
            return ["status" => 0, "message" => "Server error !"];
        }
    }

    public function updatePublishStatus($input)
    {
        try
        {
            //$user = $usersModel->getLocalUserById($id);
            //if (!$user) return ["status" => 2, "message" => "User not found !"];

            $publishModel = new Publish();
            $itemSaved = $publishModel->updatePublishStatus( $input["id"], $input["status"] );

            if (!$itemSaved) return ["status" => 3, "message" => "There was a problem while doing the update !"];

            return ["status" => 1, "message" => "updated success !", "result" => $publishModel->modelFormat( $itemSaved ) ];   

        } catch ( \Exception $e ) {
            return ["status" => 0, "message" => "Server error !"];
        }
    }

    public function activatePublish($input)
    {
        try
        {
            $publishModel = new Publish();
            $itemSaved = $publishModel->setPublishActive( $input["id"] );

            if (!$itemSaved) return ["status" => 3, "message" => "There was a problem while doing the update !"];

            $usersModel = New Users();
            $user = $usersModel->getLocalUserById( $input["uid"] );

            $this->sendPublishConfirmationEmail($user, $itemSaved);

            return ["status" => 1, "message" => "updated success !", "result" => $publishModel->modelFormat( $itemSaved ) ];   

        } catch ( \Exception $e ) {
            return ["status" => 0, "message" => "Server error !"];
        }
    }

    public function sendPublishConfirmationEmail($user, $itemSaved)
    {
        $notify = new Email();
        $emailResult = $notify->sendPublishConfirmationEmail(
            $user->email, 
            $this->config->application['name'], 
            'emails/users/publish_confirmation', 
            $this->config->email->replyTo, 
            $itemSaved
        );

        if ($emailResult["status"] != 1) {
            return ['status' => 4, 'message' => 'email error!'];
        }

        return ['status' => 1, 'message' => 'Confirmation sent!'];
    }

    public function sendPublishToConfirmEmail($user, $itemSaved)
    {
        $notify = new Email();
        $emailResult = $notify->sendPublishToConfirmEmail(
            $user->email, 
            $this->config->application['name'], 
            'emails/users/publish_to_confirm', 
            $this->config->email->replyTo, 
            $itemSaved
        );

        if ($emailResult["status"] != 1) {
            return ['status' => 4, 'message' => 'email error!'];
        }

        return ['status' => 1, 'message' => 'Confirmation sent!'];
    }

    public function removeUserPublish($uid, $itemId)
    {
        $usersModel = New Users();
        $user = null;
        try
        {
            //$user = $usersModel->getLocalUserById($id);
            //if (!$user) return ["status" => 2, "message" => "User not found !"];

            $userProfileModel = New UserProfile();
            $userProfile = $userProfileModel->getByUid( $uid );
            
            if (!$userProfile) return ["status" => 2, "message" => "User Profile not found !"];

            $publishModel = new Publish();
            $item = $publishModel->getById($itemId);
            
            $userProfile->removeAnnounce( (string) $item->_id );
            
            $userProfileModel->updateUserProfile($userProfile);

            $item->delete();

            return ["status" => 1, "message" => "updated success !" ];   

        } catch ( \Exception $e ) {
            return ["status" => 0, "message" => "Server error !"];
        }
    }

    public function deletePublish($id)
    {
        try
        {
            //$user = $usersModel->getLocalUserById($id);
            //if (!$user) return ["status" => 2, "message" => "User not found !"];

            $publishModel = new Publish();
            $item = $publishModel->getById($id);

            $userProfileModel = New UserProfile();
            $userProfile = $userProfileModel->getByUid( (string) $item->uid );
            
            if (!$userProfile) return ["status" => 2, "message" => "User Profile not found !"];
            
            $userProfile->removeAnnounce( (string) $item->_id );
            
            $userProfileModel->updateUserProfile($userProfile);

            $item->delete();

            return ["status" => 1, "message" => "updated success !" ];   

        } catch ( \Exception $e ) {
            return ["status" => 0, "message" => "Server error !"];
        }
    }

    public function getAllPublishList()
    {
        try
        {
            $items = Publish::aggregate([
                [
                    '$lookup' =>
                    [
                        'from' => 'users',
                        'localField' => 'uid',
                        'foreignField' => '_id',
                        'as' => 'user'
                    ],
                ],
                [
                    '$unwind' => '$user'
                ],
                [
                    '$lookup' =>
                    [
                        'from' => 'user_profile',
                        'localField' => 'uid',
                        'foreignField' => 'uid',
                        'as' => 'profile'
                    ],
                ],
                [
                    '$unwind' => '$profile'
                ]
            ]);

            $itemsFound = $items->toArray();
        } catch ( \Exception $e ) {
            return false;
        }
        
        if (!$itemsFound) 
        {
            return ["status" => 1, "message" => "success !", "result" => array() ];
        }

        $model = new Publish();
        $itemsToReturn = $model->modelFormatOnListForSearch($itemsFound);

        return ["status" => 1, "message" => "success !", "result" => $itemsToReturn ];
    }

    public function getBySearch($params) 
    {
        $firstMatch = [
            '$match' => 
            [
                'status' => 1
            ]
        ];

        if ($params["price"] && $params["price"] !== null) 
        {
            $paramObjPrice = json_decode($params["price"]);
            $priceValueMin = floatval($paramObjPrice[1]);
            $priceValueMax = floatval($paramObjPrice[0]);
            if ($priceValueMax > 0) {
                $firstMatch['$match']['price']['$lte'] = $priceValueMax;
            }
            if ($priceValueMin > 0) {
                $firstMatch['$match']['price']['$gte'] = $priceValueMin;
            }
        }

        if ($params["what"] && $params["what"] !== null) 
        {
            $firstMatch['$match']['what'] = ['$eq' => intval($params["what"]) ];
        }

        if ($params["chambres"] && $params["chambres"] !== null) 
        {
            $firstMatch['$match']['cham'] = ['$eq' => intval($params["chambres"]) ];
        }

        if ($params["pieces"] && $params["pieces"] !== null) 
        {
            $firstMatch['$match']['pie'] = ['$eq' => intval($params["pieces"]) ];
        }

        if ($params["habitation"] && $params["habitation"] !== null) 
        {
            $paramArray = json_decode($params["habitation"]);
            $firstMatch['$match']['habitation'] = [ 
                '$in' => $paramArray 
            ];
        }

        if ($params["location"] && $params["location"] !== null) 
        {
            $paramArray = json_decode($params["location"]);
            $firstMatch['$match']['location'] = [ 
                '$in' => $paramArray 
            ];
        }

        if ($params["surface"] && $params["surface"] !== null) 
        {
            $paramObj = json_decode($params["surface"]);
            $surfaceValueMin = floatval($paramObj[1]);
            $surfaceValueMax = floatval($paramObj[0]);
            if ($surfaceValueMax > 0) {
                $firstMatch['$match']['surf']['$lte'] = $surfaceValueMax;
            }
            if ($surfaceValueMin > 0) {
                $firstMatch['$match']['surf']['$gte'] = $surfaceValueMin;
            }
        }

        if ($params["loc"] && $params["loc"] !== null) 
        {
            $paramArrayLoc = json_decode($params["loc"]);

            $villeList = array();
            $communeList = array();
            $quartierList = array();
            $specificList = array();
            foreach($paramArrayLoc as $loc) {
                if ($loc->type === 2) {
                    $villeList[] = $loc->value;
                }

                if ($loc->type === 3) {
                    $communeList[] = $loc->value;
                }

                if ($loc->type === 4) {
                    $quartierList[] = $loc->value;
                }

                if ($loc->type === 5) {
                    $valuesLoc = $loc->value;
                    $specificList[] = $valuesLoc;
                }
            }
            
            $firstMatch['$match']['$or'] = array();
            if (count($villeList) > 0) {
                $firstMatch['$match']['$or'][] = ['ville' => [ 
                    '$in' => $villeList
                ]];
            }
            if (count($communeList) > 0) {
                $firstMatch['$match']['$or'][] = ['commune' => [ 
                    '$in' => $communeList
                ]];
            }
            if (count($quartierList) > 0) {
                $firstMatch['$match']['$or'][] = ['quartier' => [ 
                    '$in' => $quartierList
                ]];
            }
            if (count($specificList) > 0) {
                foreach($specificList as $specific) {
                    $and = ['$and' => array()];
                    if (count($specific) === 3) {
                        $and['$and'][] = [ 
                            'ville' => ['$in' => array( $specific[0] ) ],
                            'commune' => ['$in' => array( $specific[1] ) ],
                            'quartier' => ['$in' => array( $specific[2] ) ],
                        ];
                    }
                    if (count($specific) === 2) {
                        $and['$and'][] = [ 
                            'ville' => ['$in' => array( $specific[0] ) ],
                            'commune' => ['$in' => array( $specific[1] ) ]
                        ];
                    }
                    $firstMatch['$match']['$or'][] = $and;
                }

            }
        }

        //echo json_encode($firstMatch);
        try
        {
            $items = Publish::aggregate([
                $firstMatch,
                [
                    '$lookup' =>
                    [
                        'from' => 'users',
                        'localField' => 'uid',
                        'foreignField' => '_id',
                        'as' => 'user'
                    ],
                ],
                [
                    '$unwind' => '$user'
                ],
                [
                    
                    '$match' => 
                    [
                        'user.status' => 1
                    ]
                ],
                [
                    '$lookup' =>
                    [
                        'from' => 'user_profile',
                        'localField' => 'uid',
                        'foreignField' => 'uid',
                        'as' => 'profile'
                    ],
                ],
                [
                    '$unwind' => '$profile'
                ],
                [
                    '$sort' => [
                        "created_at" => -1
                    ]
                ]
            ]);

            $itemsFound = $items->toArray();
        } catch ( \Exception $e ) {
            return false;
        }
        
        if (!$itemsFound) 
        {
            return ["status" => 1, "message" => "success !", "result" => array() ];
        }

        $model = new Publish();
        $itemsToReturn = $model->modelFormatOnListForSearch($itemsFound);

        return ["status" => 1, "message" => "success !", "result" => $itemsToReturn ];
    }
    
    /*
    public function getActiveUsersWithSubscriptions()
    {
        try
        {

            $users = Users::aggregate([
                [ 
                    '$match' => [ 
                        '$id' => ObjectId($id)
                    ] 
                ],
                [
                    '$lookup' =>
                    [
                        'from' => 'user_profile',
                        'localField' => '_id',
                        'foreignField' => 'uid',
                        'as' => 'profile'
                    ]
                ]
            ]);

            
            $users = Users::aggregate([
                [
                    '$lookup' =>
                    [
                        'from' => 'user_subscriptions',
                        'localField' => '_id',
                        'foreignField' => 'uid',
                        'as' => 'subscriptions'
                    ]
                ]
            ]);
            $usersFound = $users->toArray();
        } catch ( \Exception $e ) {
            return false;
        }
        
        if (!$usersFound) return [];

        $usersModel = new Users();
        $userSubscriptionsModel = new UserSubscriptions();
        return array_map(
            function($item) use ($usersModel, $userSubscriptionsModel)
        {
            $userFormatted = $usersModel->modelFormatMajor($item);
            if (count($item->subscriptions) > 0 ) {
                $subscriptionFormatted = $userSubscriptionsModel->modelFormat(@$item->subscriptions[0]);
                $userFormatted["subscription"] = $subscriptionFormatted;
            } else {
                $subscriptionFormatted = $userSubscriptionsModel->modelFormat($userSubscriptionsModel);
                $userFormatted["subscription"] = $subscriptionFormatted;
            }
            return $userFormatted;
        }, 
        $usersFound);

        //$podcastsFormatted = $podcastModel->modelFormatOnList($podcastsFound);
        //return $podcastsFormatted;
    }
    */

}