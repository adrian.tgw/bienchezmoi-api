<?php 
declare(strict_types=1);
namespace Presenters;

use Phalcon\Validation\Validator\MongoId;

use Models\Users;
use Models\UserProfile;
use Models\UserConfirmation;
use Models\Publish; 

use Services\Email;

class UsersPresenter
{
    public function __construct($config)
    { 
        $this->config = $config;
    }

    public function newUserIndividual($input, $secretKey)
    {
        $user = new Users();
        $user = $user->registerNewWithPassword($input, $secretKey);
        $user = $user->setUserActive( $user );

        if ( $user )
        {
            $userProfile = new UserProfile();
            $userProfileSaved = $userProfile->registerNewEmpty( (string) $user->getId() );

            //$userConfirmation = new UserConfirmation();
            //$userConfirmation->newToken( (string) $user->_id );
            $this->sendConfirmationEmail($user);

            return ['status' => 1, 'message' => 'register success!', 'result' => $user->modelFormat($user)];
        } 
        else
        {
            return ['status' => 4, 'message' => 'database error!'];
        }
    }

    public function newUserSocial($input, $secretKey)
    {
        $user = new Users();
        $user = $user->registerNewWithPasswordSocial($input, $secretKey);
        $user = $user->setUserActive( $user );

        if ( $user )
        {
            $userProfile = new UserProfile();
            $userProfileSaved = $userProfile->registerNewEmpty( (string) $user->getId() );

            //$userUpdated = $user->setUserConfirmed( $user );
            /*
            if (!$userUpdated) {
                return ['status' => 5, 'message' => 'Error on confirm user'];
            }
            */
            $this->sendConfirmationEmail($user);

            return ['status' => 1, 'message' => 'register success!', 'result' => $user->modelFormat($user)];
        } 
        else
        {
            return ['status' => 4, 'message' => 'database error!'];
        }
    }

    public function newUserApple($email, $userId, $name, $lastName)
    {
        $user = new Users();
        $user = $user->registerNewUserApple($email, $userId, $name, $lastName);
        $user = $user->setUserActive( $user );

        if ( $user )
        {
            $userProfile = new UserProfile();
            $userProfileSaved = $userProfile->registerNewEmpty( (string) $user->getId() );

            $this->sendConfirmationEmail($user);
            return $user;
        } 
        else
        {
            return false;
        }
    }

    public function sendToConfirmationEmail($user)
    {
        $notify = new Email();
        $emailResult = $notify->sendConfirmationEmail(
            $user->email, 
            $this->config->application['name'], 
            'emails/users/to-confirm-email', 
            $this->config->email->replyTo, 
            $user
        );

        if ($emailResult["status"] != 1) {
            return ['status' => 4, 'message' => 'email error!'];
        }

        return ['status' => 1, 'message' => 'Confirmation sent!'];
    }

    public function sendConfirmationEmail($user)
    {
        $notify = new Email();
        $emailResult = $notify->sendConfirmationEmail(
            $user->email, 
            $this->config->application['name'], 
            'emails/users/confirmation-email', 
            $this->config->email->replyTo, 
            $user
        );

        if ($emailResult["status"] != 1) {
            return ['status' => 4, 'message' => 'email error!'];
        }

        return ['status' => 1, 'message' => 'Confirmation sent!'];
    }

    public function getUserAndProfile($id)
    {
        $usersModel = New Users();
        $user = null;
        try
        {
            $user = $usersModel->getLocalUserById($id);

            $userProfileModel = New UserProfile();
            $userProfile = $userProfileModel->getByUid( (string) $user->getId() );
            $user->profile =  $userProfileModel->modelFormat( $userProfile );
            
        } catch ( \Exception $e ) {
            return ["status" => 0, "message" => "Server error !"];
        }
        
        if (!$user) return ["status" => 2, "message" => "User not found !"];

        return ["status" => 1, "message" => "User found !", "result" => $usersModel->modelFormat( $user ) ];
    }

    public function getUsersAndProfiles()
    {

        try
        {
            $items = Users::aggregate([
                [
                    '$lookup' =>
                    [
                        'from' => 'user_profile',
                        'localField' => '_id',
                        'foreignField' => 'uid',
                        'as' => 'profile'
                    ],
                ],
                [
                    '$unwind' => '$profile'
                ]
            ]);

            $itemsFound = $items->toArray();
        } catch ( \Exception $e ) {
            return false;
        }
        
        if (!$itemsFound) return ["status" => 2, "message" => "User not found !"];

        $usersModel = new Users();
        return ["status" => 1, "message" => "User found !", "result" => $usersModel->modelFormatOnList( $itemsFound ) ];
    }

    public function getUser($id)
    {
        $usersModel = New Users();
        try
        {
            $user = $usersModel->getLocalUserById($id);
            return $user;
            
        } catch ( \Exception $e ) {
            return false;
        }
        
        return false;
    }

    public function setUserActiveState($id, $toActive)
    {
        $usersModel = New Users();
        $user = null;
        try
        {
            $user = $usersModel->getLocalUserById($id);

            if (!$user) return ["status" => 2, "message" => "User not found !"];

            if ($toActive) {
                $user = $usersModel->setUserActive( $user );
            } else {
                $user = $usersModel->setUserDeactive( $user );
            }

        } catch ( \Exception $e ) {
            return ["status" => 0, "message" => "Server error !"];
        }

        if (!$user) return ["status" => 3, "message" => "There was a problem while doing the update !"];

        $this->sendActiveEmail($user, $toActive);
        return ["status" => 1, "message" => "updated success !", "result" => $usersModel->modelFormat( $user ) ];
    }

    public function setUserPassword($id, $input, $secretKey)
    {
        $usersModel = New Users();
        $user = null;
        try
        {
            $user = $usersModel->getLocalUserById($id);

            if (!$user) return ["status" => 2, "message" => "User not found !"];

            $user = $user->updateUserPassword($input["password"], $secretKey);

        } catch ( \Exception $e ) {
            return ["status" => 0, "message" => "Server error !"];
        }

        if (!$user) return ["status" => 3, "message" => "There was a problem while doing the update !"];

        return ["status" => 1, "message" => "updated success !", "result" => $usersModel->modelFormat( $user ) ];
    }

    public function setUserInfo($id, $input)
    {
        $usersModel = New Users();
        $user = null;
        try
        {
            $user = $usersModel->getLocalUserById($id);

            if (!$user) return ["status" => 2, "message" => "User not found !"];

            $user = $user->setUserInfo($user, $input);

        } catch ( \Exception $e ) {
            return ["status" => 0, "message" => "Server error !"];
        }

        if (!$user) return ["status" => 3, "message" => "There was a problem while doing the update !"];

        return ["status" => 1, "message" => "updated success !", "result" => $usersModel->modelFormat( $user ) ];
    }
    
    public function sendActiveEmail($user, $isActive)
    {
        $notify = new Email();
        $emailResult = $notify->sendActiveEmail(
            $user->email, 
            $this->config->application['name'], 
            'emails/users/active-deactive-email', 
            $this->config->email->replyTo, 
            $user,
            $isActive
        );

        if ($emailResult["status"] != 1) {
            return ['status' => 4, 'message' => 'email error!'];
        }

        return ['status' => 1, 'message' => 'Active sent!'];
    }
    
    public function newUserProfessional($input, $secretKey)
    {
        $user = new Users();
        $user = $user->registerNewWithPassword($input, $secretKey);

        if ( $user )
        {
            $userProfile = new UserProfile();
            $userProfileSaved = $userProfile->registerNew( (string) $user->getId(), $input );

            $this->sendToConfirmationEmail($user );

            return ['status' => 1, 'message' => 'register success!', 'result' => $user->modelFormat($user)];
        } 
        else
        {
            return ['status' => 4, 'message' => 'database error!'];
        }
    }

    public function setUserProfile($uid, $input)
    {
        $userProfile = new UserProfile();
        $userProfileSaved = $userProfile->registerUserValues( $uid, $input );

        if ($userProfileSaved) 
        {
            return ['status' => 1, 'message' => 'register success!', 'result' => $userProfile->modelFormat($userProfileSaved)];
        }
        else
        {
            return ['status' => 4, 'message' => 'database error!'];
        }
    }
    
    public function updateProfile($uid, $input)
    {
        $userProfile = new UserProfile();
        $userProfileSaved = $userProfile->updateValues( $uid, $input );

        if ($userProfileSaved) 
        {
            return ['status' => 1, 'message' => 'register success!', 'result' => $userProfile->modelFormat($userProfileSaved)];
        }
        else
        {
            return ['status' => 4, 'message' => 'database error!'];
        }
    }
    
    /*
    public function getActiveUsersWithSubscriptions()
    {
        try
        {

            $users = Users::aggregate([
                [ 
                    '$match' => [ 
                        '$id' => ObjectId($id)
                    ] 
                ],
                [
                    '$lookup' =>
                    [
                        'from' => 'user_profile',
                        'localField' => '_id',
                        'foreignField' => 'uid',
                        'as' => 'profile'
                    ]
                ]
            ]);

            
            $users = Users::aggregate([
                [
                    '$lookup' =>
                    [
                        'from' => 'user_subscriptions',
                        'localField' => '_id',
                        'foreignField' => 'uid',
                        'as' => 'subscriptions'
                    ]
                ]
            ]);
            $usersFound = $users->toArray();
        } catch ( \Exception $e ) {
            return false;
        }
        
        if (!$usersFound) return [];

        $usersModel = new Users();
        $userSubscriptionsModel = new UserSubscriptions();
        return array_map(
            function($item) use ($usersModel, $userSubscriptionsModel)
        {
            $userFormatted = $usersModel->modelFormatMajor($item);
            if (count($item->subscriptions) > 0 ) {
                $subscriptionFormatted = $userSubscriptionsModel->modelFormat(@$item->subscriptions[0]);
                $userFormatted["subscription"] = $subscriptionFormatted;
            } else {
                $subscriptionFormatted = $userSubscriptionsModel->modelFormat($userSubscriptionsModel);
                $userFormatted["subscription"] = $subscriptionFormatted;
            }
            return $userFormatted;
        }, 
        $usersFound);

        //$podcastsFormatted = $podcastModel->modelFormatOnList($podcastsFound);
        //return $podcastsFormatted;
    }
    */

}