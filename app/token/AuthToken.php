<?php namespace Token;

ini_set('default_socket_timeout', 3600);

use Models\UserToken;
use Firebase\JWT\JWT;

class AuthToken {

    private $TOKEN_TIME = '+8 week';
    private $tokenKey = 'mel3jlHS@w$IJ3Fatr!j_se';

    public function __construct()
    { }

    public function getTokenTime() 
    {
        return $this->TOKEN_TIME;
    }

    public function newToken($id)
    {
        $uToken = new UserToken();
        
        $newToken = $this->generateToken($id);
        
        $token = $uToken->getToken(['uid' => (string) $id]);
        $token->uid = (string) $id;
        $token->token = $newToken;
        $token->created_at = time();
        $token->expires_at = strtotime($this->TOKEN_TIME);
        $token->save();

        return $newToken;
    }

    public function newTokenAnonymous($id)
    {
        $uToken = new UserToken();
        
        $newToken = $this->generateToken("");
        
        $token = new UserToken();
        $token->uid = (string) $id;
        $token->token = $newToken;
        $token->anonymous = true;
        $token->created_at = time();
        $token->expires_at = strtotime($this->TOKEN_TIME);
        $token->save();

        return $newToken;
    }

    public function exists($uid)
    {
        $uToken = new UserToken();
        return $uToken->getToken(['uid' => (string) $uid]);
    }

    public function getToken($uid)
    {
        return $this->exists($uid);
    }

    protected function generateToken($id)
    {
        $token = array(
            "iss" => "http://ar.dev",
            "aud" => "http://ar.dev",
            "uid" => (string) $id,
            "iat" => time(),
            "exp" => strtotime($this->TOKEN_TIME)
        );

        $jwt = JWT::encode($token, $this->tokenKey);

        return $jwt;
    }

    protected function generateTokenAnonymous($id)
    {
        $token = array(
            "iss" => "http://ar.dev",
            "aud" => "http://ar.dev",
            "uid" => (string) $id,
            "anonymous" => true,
            "iat" => time(),
            "exp" => strtotime($this->TOKEN_TIME)
        );

        $jwt = JWT::encode($token, $this->tokenKey);

        return $jwt;
    }

}
