<?php namespace Services;

use Services\MRequests;
use Models\FlashMessage;
use Models\ExpoPushNotification;

class MPushNotifications 
{
    private $maxLimitPerRequest = 100;
    private $isTest;
    private $expoEndPoint = 'https://exp.host/--/api/v2/push/send';

    public function __construct($isTest = false)
    {
        $this->isTest = $isTest;
    }

    public function sendMultipleNotifications($userDevices, $flashMessage)
    {
        $deviceMessages = [];
        $usersUpdated = [];
        
        $totalResults = [
            'total' => 0,
            'success' => 0,
            'error' => 0,
            'totalrequests' => 0
        ];

        foreach ( $userDevices as $user ) {
            
            $newMessage = $this->getNewMessagePush( $flashMessage );
            $userMessages = $this->getUserPushMessages($user, $newMessage);
            $badges = count($userMessages);

            $userIDuo = $user->getID().'';

            $userUpdated = $this->updateUserBadgeAndMessages($user, $badges, $userMessages);

            foreach($user->devices as $device) {
                //$device = $user->devices[0];
                $notification = new ExpoPushNotification(
                    $device['token'],
                    $flashMessage->title,
                    $flashMessage->body
                );
                $notification->setNumBadge($badges);
    
                $notification->setData([
                    'title' => $flashMessage->title, 
                    'body' => $flashMessage->body, 
                    'active' => true, 
                    'id' => $flashMessage->idReference, 
                    'badges' => $badges, 
                    'uid' => $userIDuo
                ]);

                $usersUpdated[] = $userUpdated;
                
                $deviceMessages[] = $notification->getJSONObj();
                if ( count($deviceMessages) >= $this->maxLimitPerRequest ) {
                    try {
                        $totalResults = $this->sendNotificationsLogic($deviceMessages, 
                        $totalResults, $usersUpdated);
    
                        unset($deviceMessages);
                        $deviceMessages = array();
    
                        unset($usersUpdated);
                        $usersUpdated = array();
    
                    } catch ( \Exception $e ) {
                        var_dump($e->getMessage());
                        return;
                    }
                }
            }

        }

        if (!empty($deviceMessages))
        {
            try {
                $totalResults = $this->sendNotificationsLogic($deviceMessages, 
                $totalResults, $usersUpdated);

                unset($deviceMessages);
                $deviceMessages = array();

                unset($usersUpdated);
                $usersUpdated = array();
                
            } catch ( \Exception $e ) {
                var_dump($e->getMessage());
                return;
            }
        }

        return ['status' => 1, 'message' => 'ok', 'results' => $totalResults];
    }

    protected function getBadgesCount($user)
    {
        $badges = 0;
        if ( isset($user->badges) ) {
            $badges = $user->badges;
        }
        
        $badges += 1;

        return $badges;
    }

    protected function getNewMessagePush($flashMessage)
    {
        $new_message = [];
        if ($flashMessage->idReference) {
            $new_message['id'] = $flashMessage->idReference;
        } else {
            $new_message['id'] = NULL;
        }
                
        if ($flashMessage->flashID) {
            $new_message['flashid'] = $flashMessage->flashID;
        } else {
            $new_message['flashid'] = NULL;
        }

        $new_message['seen'] = false;

        return $new_message;
    }

    protected function getUserPushMessages( $user, $new_message )
    {
        $messages = [ ];
        if (isset($user->push_messages)) 
        {
            $messages = $user->push_messages;
        }

        $found = false;
        foreach($messages as $message)
        {
            if ($new_message['id'] == $message['id']) 
            {
                $found = true;
            }
        }
        
        if ($new_message['id'] !== NULL && !$found)
        {
            $messages[] = $new_message;
        }

        return $messages;
    }

    protected function updateUserBadgeAndMessages( $user, $badges, $userMessages )
    {        
        $values = [
            "badges" => count($userMessages),
            "push_messages" => $userMessages
        ];

        foreach ( $values as $key=>$val ) {
            $user->$key = $val;
        }

        return $user;
    }

    protected function sendNotificationsLogic( $totalDevices, $totalResults, $usersUpdated )
    {
        $resultNotifications = $this->sendNotifications($totalDevices);
        $usersUpdatedResults = $this->updateUsersWithNewPushNotification($resultNotifications, $usersUpdated);
        $totalResults = $this->parseUsersUpdatedResults($usersUpdatedResults, $totalResults);

        return $totalResults;
    }

    protected function sendNotifications($totalDevices)
    {
        if ($this->isTest) 
        {
            return $this->sendNotificationsTest($totalDevices);
        }

        $requests = new MRequests();
        $response = $requests->postRequestJSON(json_encode($totalDevices), $this->expoEndPoint);
        return $response;
    }

    protected function sendNotificationsTest($totalDevices)
    {
        $result = [
            'data' => [ ]
        ];
            
        for($index = 0; $index < count($totalDevices); ++$index)
        { 
            $result['data'][] = [ 'status' => 'ok', 'message' => 'success'];
        }

        $response = ['response' => json_encode($result)];
        return $response;
    }

    protected function updateUsersWithNewPushNotification($resultNotifications, $usersUpdated)
    {
        $resultJSON = json_decode($resultNotifications['response']);
        $resultsUpdated = [];

        $index = 0;
        foreach( $resultJSON->data as $result )
        {
            $userUpdated = $usersUpdated[$index];
            $status = $result->status;
            $message = 'Ok';

            if ($status === 'ok')
            {
                if ($this->isTest)
                {
                    $resultsUpdated[] = [
                        'status' => 1,
                        'messages' => 'success'
                    ];
                } else {

                    if ( $userUpdated->save() ) {
                        $resultsUpdated[] = [
                            'status' => 1,
                            'messages' => 'success'
                        ];
                    } else {
                        $resultsUpdated[] = [
                            'status' => 0,
                            'messages' => 'error'
                        ];
                    }
                    
                }
            } else {
                $resultsUpdated[] = [
                    'status' => 2,
                    'messages' => 'error'
                ];
            }
            // $totalResults[] = $this->newNotificationResult($status, $message, $user);
            ++$index;
        }

        return $resultsUpdated;
    }

    protected function newNotificationResult($status, $message, $user)
    {
        $statusNum = $status == 'ok' ? 1 : 0;
        return [
            'status' => $statusNum,
            'message' => $message,
            'badges' => $user->badges,
            'push_messages' => $user->push_messages
        ];
    }

    protected function parseUsersUpdatedResults($usersUpdatedResults, $totalResults)
    {
        $total = $totalResults['total'];
        $success = $totalResults['success'];
        $error = $totalResults['error'];
        $totalRequests = $totalResults['totalrequests'];
        $totalRequests += 1;

        foreach( $usersUpdatedResults as $result )
        {
            $total += 1;
            
            if ($result['status'] == 1)
            {
                $success += 1;
            } else {
                $error += 1;
            }
        }

        $totalResults['totalrequests'] = $totalRequests;
        
        $totalResults['total'] = $total;
        $totalResults['success'] = $success;
        $totalResults['error'] = $error;

        return $totalResults;
    }

}
