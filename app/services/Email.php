<?php namespace Services;

use Aws\Ses\SesClient;
use Aws\Exception\AwsException;
use Aws\S3\Exception\SESException;

use Phalcon\Mvc\View\Simple as SimpleView;

class Email extends BaseServices {

    public function sendConfirmationEmail(
        $destination, 
        $appName,
        $viewRender,
        $replyTo,
        $user
    ) 
    {
        $input = [];
        $subject = "Confirmation d'inscription";
        try {
            $SesClient = SesClient::factory(array(
                'credentials' => [
                    'key'     => $this->config['aws']['key'],
                    'secret'  => $this->config['aws']['secret'],
                ],
                'region'  => $this->config['aws']['region'],
                'version' => $this->config['aws']['version']
            ));

            $view = new SimpleView();
            $view->setViewsDir($this->config['application']['viewsDir']);
            $viewHtml = $view->render(
                $viewRender, 
                [
                    "name" => $user->name,
                    "email" => $destination,
                    "appName" => $appName,
                    "lastName" => $user->lastName,
                    "urlLogo" => "http://api.appbienchezmoi.com/img/app/bienchezmoi_logo.png"
                ]
            );
            
            $viewPlainText = strip_tags($viewHtml);

            $sender_email = 'support@appbienchezmoi.com';
            
            $recipient_emails = [$destination];

            $subject = $subject;
            $plaintext_body = 'Email de confirmation: ';
            $html_body =  $viewHtml;
            $char_set = 'UTF-8';
            
            try {
                $result = $SesClient->sendEmail([
                    'Destination' => [
                        'ToAddresses' => $recipient_emails,
                    ],
                    'ReplyToAddresses' => [$sender_email],
                    'Source' => $appName."<".$sender_email.">",
                    'From' => $appName,
                    'Message' => [
                    'Body' => [
                        'Html' => [
                            'Charset' => $char_set,
                            'Data' => $html_body,
                        ],
                        'Text' => [
                            'Charset' => $char_set,
                            'Data' => $plaintext_body,
                        ],
                    ],
                    'Subject' => [
                        'Charset' => $char_set,
                        'Data' => $subject,
                    ],
                ],
                ]);
                $messageId = $result['MessageId'];
                return ['status' => 1, 'message' => 'email sent!'];
            } catch (AwsException $e) {
                return ['status' => 0, 'message' => $e->getMessage(), 'errorMessage' => $e->getAwsErrorMessage()];
            }
            
        } catch(Exception $e) {
            return ['status' => 4, 'message' => "server error"];
        }
    }

    public function sendPublishConfirmationEmail(
        $destination, 
        $appName,
        $viewRender,
        $replyTo,
        $itemSaved
    ) 
    {
        $input = [];
        $subject = "Votre annonce ".$itemSaved->short." est en ligne";
        try {
            $SesClient = SesClient::factory(array(
                'credentials' => [
                    'key'     => $this->config['aws']['key'],
                    'secret'  => $this->config['aws']['secret'],
                ],
                'region'  => $this->config['aws']['region'],
                'version' => $this->config['aws']['version']
            ));

            $view = new SimpleView();
            $view->setViewsDir($this->config['application']['viewsDir']);
            $viewHtml = $view->render(
                $viewRender, 
                [
                    "email" => $destination,
                    "appName" => $appName,
                    "urlLogo" => "http://api.appbienchezmoi.com/img/app/bienchezmoi_logo.png"
                ]
            );
            
            $viewPlainText = strip_tags($viewHtml);

            $sender_email = 'support@appbienchezmoi.com';
            
            $recipient_emails = [$destination];

            $subject = $subject;
            $plaintext_body = "Votre annonce ".$itemSaved->short." est en ligne";
            $html_body =  $viewHtml;
            $char_set = 'UTF-8';
            
            try {
                $result = $SesClient->sendEmail([
                    'Destination' => [
                        'ToAddresses' => $recipient_emails,
                    ],
                    'ReplyToAddresses' => [$sender_email],
                    'Source' => $appName."<".$sender_email.">",
                    'From' => $appName,
                    'Message' => [
                    'Body' => [
                        'Html' => [
                            'Charset' => $char_set,
                            'Data' => $html_body,
                        ],
                        'Text' => [
                            'Charset' => $char_set,
                            'Data' => $plaintext_body,
                        ],
                    ],
                    'Subject' => [
                        'Charset' => $char_set,
                        'Data' => $subject,
                    ],
                ],
                ]);
                $messageId = $result['MessageId'];
                return ['status' => 1, 'message' => 'email sent!'];
            } catch (AwsException $e) {
                return ['status' => 0, 'message' => $e->getMessage(), 'errorMessage' => $e->getAwsErrorMessage()];
            }
            
        } catch(Exception $e) {
            return ['status' => 4, 'message' => "server error"];
        }
    }

    public function sendPublishToConfirmEmail(
        $destination, 
        $appName,
        $viewRender,
        $replyTo,
        $itemSaved
    ) 
    {
        $input = [];
        $subject = "Votre annonce ".$itemSaved->short." a été prise en compte";
        try {
            $SesClient = SesClient::factory(array(
                'credentials' => [
                    'key'     => $this->config['aws']['key'],
                    'secret'  => $this->config['aws']['secret'],
                ],
                'region'  => $this->config['aws']['region'],
                'version' => $this->config['aws']['version']
            ));

            $view = new SimpleView();
            $view->setViewsDir($this->config['application']['viewsDir']);
            $viewHtml = $view->render(
                $viewRender, 
                [
                    "email" => $destination,
                    "appName" => $appName,
                    "urlLogo" => "http://api.appbienchezmoi.com/img/app/bienchezmoi_logo.png"
                ]
            );
            
            $viewPlainText = strip_tags($viewHtml);

            $sender_email = 'support@appbienchezmoi.com';
            
            $recipient_emails = [$destination];

            $subject = $subject;
            $plaintext_body = "Votre annonce ".$itemSaved->short." a été prise en compte";
            $html_body =  $viewHtml;
            $char_set = 'UTF-8';
            
            try {
                $result = $SesClient->sendEmail([
                    'Destination' => [
                        'ToAddresses' => $recipient_emails,
                    ],
                    'ReplyToAddresses' => [$sender_email],
                    'Source' => $appName."<".$sender_email.">",
                    'From' => $appName,
                    'Message' => [
                    'Body' => [
                        'Html' => [
                            'Charset' => $char_set,
                            'Data' => $html_body,
                        ],
                        'Text' => [
                            'Charset' => $char_set,
                            'Data' => $plaintext_body,
                        ],
                    ],
                    'Subject' => [
                        'Charset' => $char_set,
                        'Data' => $subject,
                    ],
                ],
                ]);
                $messageId = $result['MessageId'];
                return ['status' => 1, 'message' => 'email sent!'];
            } catch (AwsException $e) {
                return ['status' => 0, 'message' => $e->getMessage(), 'errorMessage' => $e->getAwsErrorMessage()];
            }
            
        } catch(Exception $e) {
            return ['status' => 4, 'message' => "server error"];
        }
    }

    public function sendActiveEmail(
        $destination, 
        $appName,
        $viewRender,
        $replyTo,
        $user,
        $isActive
    ) 
    {
        $input = [];
        $subject = "Statut du compte";
        try {
            $SesClient = SesClient::factory(array(
                'credentials' => [
                    'key'     => $this->config['aws']['key'],
                    'secret'  => $this->config['aws']['secret'],
                ],
                'region'  => $this->config['aws']['region'],
                'version' => $this->config['aws']['version']
            ));

            
            $view = new SimpleView();
            $view->setViewsDir($this->config['application']['viewsDir']);
            $viewHtml = $view->render(
                $viewRender, 
                [
                    "name" => $user->name,
                    "email" => $destination,
                    "appName" => $appName,
                    "lastName" => $user->lastName,
                    "active" => $isActive ? "activé" : "désactivé",
                    "urlLogo" => "http://api.appbienchezmoi.com/img/app/bienchezmoi_logo.png"
                ]
            );
            
            $viewPlainText = strip_tags($viewHtml);

            $sender_email = 'support@appbienchezmoi.com';
            
            $recipient_emails = [$destination];

            $subject = $subject;
            $plaintext_body = 'Email de activation: ';
            $html_body =  $viewHtml;
            $char_set = 'UTF-8';
            
            try {
                $result = $SesClient->sendEmail([
                    'Destination' => [
                        'ToAddresses' => $recipient_emails,
                    ],
                    'ReplyToAddresses' => [$sender_email],
                    'Source' => $appName."<".$sender_email.">",
                    'From' => $appName,
                    'Message' => [
                    'Body' => [
                        'Html' => [
                            'Charset' => $char_set,
                            'Data' => $html_body,
                        ],
                        'Text' => [
                            'Charset' => $char_set,
                            'Data' => $plaintext_body,
                        ],
                    ],
                    'Subject' => [
                        'Charset' => $char_set,
                        'Data' => $subject,
                    ],
                ],
                ]);
                $messageId = $result['MessageId'];
                return ['status' => 1, 'message' => 'email sent!'];
            } catch (AwsException $e) {
                return ['status' => 0, 'message' => $e->getMessage(), 'errorMessage' => $e->getAwsErrorMessage()];
            }
            
        } catch(Exception $e) {
            return ['status' => 4, 'message' => "server error"];
        }
    }

    public function sendRecoveryEmail(
        $destination, 
        $appName,
        $viewRender,
        $replyTo,
        $user,
        $rememberToken
    ) 
    {
        $input = [];
        $subject = "Récupération de mot de passe";
        try {
            $SesClient = SesClient::factory(array(
                'credentials' => [
                    'key'     => $this->config['aws']['key'],
                    'secret'  => $this->config['aws']['secret'],
                ],
                'region'  => $this->config['aws']['region'],
                'version' => $this->config['aws']['version']
            ));

            $view = new SimpleView();
            $view->setViewsDir($this->config['application']['viewsDir']);
            $viewHtml = $view->render(
                $viewRender, 
                [
                    "rememberToken" => $rememberToken,
                    "urlLogo" => "http://api.appbienchezmoi.com/img/app/bienchezmoi_logo.png"
                ]
            );

            $viewPlainText = strip_tags($viewHtml);

            $sender_email = 'support@appbienchezmoi.com';
            
            $recipient_emails = [$destination];

            $subject = $subject;
            $plaintext_body = 'Récupération de mot de passe: ';
            $html_body =  $viewHtml;
            $char_set = 'UTF-8';

            try {
                $result = $SesClient->sendEmail([
                    'Destination' => [
                        'ToAddresses' => $recipient_emails,
                    ],
                    'ReplyToAddresses' => [$sender_email],
                    'Source' => $appName."<".$sender_email.">",
                    'Message' => [
                    'Body' => [
                        'Html' => [
                            'Charset' => $char_set,
                            'Data' => $html_body,
                        ],
                        'Text' => [
                            'Charset' => $char_set,
                            'Data' => $plaintext_body,
                        ],
                    ],
                    'Subject' => [
                        'Charset' => $char_set,
                        'Data' => $subject,
                    ],
                    ],
                ]);
                $messageId = $result['MessageId'];
                return ['status' => 1, 'message' => 'email sent!'];
            } catch (AwsException $e) {
                return ['status' => 0, 'message' => $e->getMessage(), 'errorMessage' => $e->getAwsErrorMessage()];
            }
            
        } catch(Exception $e) {
            return ['status' => 4, 'message' => "server error"];
        }
    }

}
