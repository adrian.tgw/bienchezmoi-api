<?php namespace Services;

use Aws\Ses\SesClient;

require_once dirname(__FILE__).'/../../vendor/rmccue/requests/library/Requests.php';
use Requests;

class MRequests {

    public function postRequestJSON($data, $endpoint)
    {
        $headers = $this->getPostHeaders();

        try {
            Requests::register_autoloader();
            $request = Requests::post($endpoint, $headers, $data);
            return ['status' => 1, 'response' => $request->body];
        } catch ( \MessageRejected $e ) {
            return ['status' => 3, 'message' => 'request rejected', 'result' => $e->getMessage()];
        }
    }

    public function postRequest($data, $endpoint)
    {
        $headers = $this->getPostHeadersUniqueMessage();

        try {
            Requests::register_autoloader();
            $request = Requests::post($endpoint, $headers, $data);
            return ['status' => 1, 'response' => $request->body];
        } catch ( \MessageRejected $e ) {
            return ['status' => 3, 'message' => 'request rejected', 'result' => $e->getMessage()];
        }
    }

    public function getRequestCustomServer($action, $params = null)
    {
        $headers = $this->getGetHeaders();

        $url = "https://appreo.techgoodways.com/reo-server-api-master/api.php" . "?" . "action=" . $action;
        
        if ($params && $params != null) {
            $url .= "&" . $params;
        }

        try {
            Requests::register_autoloader();
            $request = Requests::get($url, $headers);
            if ($request->status_code == 200) {
                return ['status' => 1, 'response' => $request->body];
            } else {
                return ['status' => 2, 'message' => "Server Error!"];
            }
        } catch ( \MessageRejected $e ) {
            return ['status' => 3, 'message' => 'request rejected', 'result' => $e->getMessage()];
        }
    }

    public function postRequestCustomServer($action, $params = null)
    {
        $headers = $this->getCustomPostHeaders();

        $url = "https://appreo.techgoodways.com/reo-server-api-master/api.php" . "?" . "action=" . $action;

        try {
            Requests::register_autoloader();
            $request = Requests::post($url, $headers, $params);
            if ($request->status_code == 200) {
                return ['status' => 1, 'response' => $request->body];
            } else {
                return ['status' => 2, 'message' => "Server Error!"];
            }
        } catch ( \MessageRejected $e ) {
            return ['status' => 3, 'message' => 'request rejected', 'result' => $e->getMessage()];
        }
    }

    private function getPostHeaders()
    {
        return [
            'accept' => 'application/json',
            'accept-encoding' => 'gzip, deflate',
            'content-type' => 'application/json',
        ];
    }

    private function getPostHeadersUniqueMessage()
    {
        return [
            'accept' => 'application/json',
            'accept-encoding' => 'gzip, deflate'
        ];
    }

    private function getGetHeaders()
    {
        return [
            'Api-Key' => 'Api-token 6102',
        ];
    }

    private function getCustomPostHeaders()
    {
        return [
            'Api-Key' => 'Api-token 6102',
            'content-type' => 'application/x-www-form-urlencoded',
        ];
    }
}
