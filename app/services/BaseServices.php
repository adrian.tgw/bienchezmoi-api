<?php namespace Services;

use Phalcon\Di\FactoryDefault;
use Phalcon\Di;

class BaseServices {

    public $config;

    public function __construct()
    {
        $di = new FactoryDefault();

        /**
         * Shared configuration service
         */
        $di->setShared('config', function () {
            return include APP_PATH . "/config/config.php";
        });

        $this->config = $di->getConfig();
    }

}
