<?php namespace Services;

class SaveImageService
{
    public function __construct()
    { }

    public function savePublishPhotos($input, $id)
    {
        $images = json_decode($input["item_images"], true);
        return $this->saveFiles($images, $id, "publish");
    }

    public function saveFlashPhotos($input, $id)
    {
        $images = json_decode($input["item_images"], true);
        return $this->saveFiles($images, $id, "flash");
    }

    public function saveFiles($itemImages, $id, $dir)
    {
        $index = -1;
        $imagesNames = array();
        foreach($itemImages as $itemImage)
        {
            $index += 1;
            if (!$itemImage["updated"]) {
                $imagesNames[] = $itemImage["file"];
                continue;
            }
            //Stores the filename as it was on the client computer.
            $fileUploaded = $_FILES['files_'.$itemImage["fileID"]];

            //Stores the filetype e.g image/jpeg
            $imagetype = $fileUploaded['type'];
            //Stores any error codes from the upload.
            $imageerror = $fileUploaded['error'];
            //Stores the tempname as it is given by the host when uploaded.
            $imagetemp = $fileUploaded['tmp_name'];

            //The path you wish to upload the image to
            $imagePath = __DIR__."/../../public/assets/" . $dir . "/";

            $imagename = $id. "_file_" . $index . "_." . pathinfo($fileUploaded['name'], PATHINFO_EXTENSION);

            if(is_uploaded_file($imagetemp)) {
                if(move_uploaded_file($imagetemp, $imagePath . $imagename)) {
                    $imagesNames[] = $imagename;
                }
            }

        }

        return $imagesNames;
    }

}