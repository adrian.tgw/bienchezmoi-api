<?php
/**
 * store simple functions that are essential to app operation
 */

/**
 * convert various dates to a common format
 *
 * @param string $date
 * @return bool|string
 */
function us_date($date = null)
{
    switch ($date) {
        case '':
        case '--':
        case '//':
        case null:
        case '0000-00-00':
        case '0000-00-00 00:00:00':
            return '';
            break;

        default:
            return date("m/d/Y", strtotime($date));
            break;
    }
}

/**
 * loop inside the array checking if each item has empty values
 * if it has, remove it fron array and return a clean array
 * */
function clearEmptyValuesFromArray($arr)
{
    $res = [];
    foreach ( $arr as $key => $val ) {
        if ( is_array($val) )
            $res[$key] = clearEmptyValuesFromArray($val);
        else {
            if ( strlen($val)>0 )
                $res[$key] = $val;
        }
    }

    // clean subarrays
    return array_filter($res);
}

if (!function_exists('dd')) {
    function dd()
    {
        $args = func_get_args();
        call_user_func_array('dump', $args);
        die();
    }
}
if (!function_exists('d')) {
    function d()
    {
        $args = func_get_args();
        call_user_func_array('dump', $args);
    }
}

/**
 * ine = Isset and Not Empty
 *
 * return @array|@object if true, if not return only false
 * example: ine($input['email']) ?: ''
 **/
function ine($list, $field=false)
{
    if ( $field===false ) {
        return 'field not informed!';
    }

    if ( is_array($list) )
        return isset($list[$field]) && strlen($list[$field]) ? $list[$field] : false;
    else if ( is_object($list) )
        return is_callable($list, false, $field) && strlen($list->$field) ? $list->$field : false;
    else
        return false;
}

/*
 * |--------------------------------------------------------------------------
 * | File Stream Modes
 * |--------------------------------------------------------------------------
 * |
 * | These modes are used when working with fopen()/popen()
 * |
 */
define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

function write_file($path, $data, $mode = FOPEN_WRITE_CREATE_DESTRUCTIVE)
{
    if (!$fp = @fopen($path, $mode)) {
        return false;
    }

    flock($fp, LOCK_EX);
    fwrite($fp, $data);
    flock($fp, LOCK_UN);
    fclose($fp);

    return true;
}

function obj2array($data)
{
    if (is_array($data) || is_object($data))
    {
        $result = array();
        foreach ($data as $key => $value)
        {
            $result[$key] = obj2array($value);
        }
        return $result;
    }
    return $data;
}

/**
 * Return current node of API
 * return @string users/search | users
 */
function currentNode($firstOnly=true)
{
    $request = parse_url($_SERVER['REQUEST_URI']);
    $path = $request["path"];

    $result = trim(str_replace(basename($_SERVER['SCRIPT_NAME']), '', $path), '/');

    $result = explode('/', $result);

    if ( $firstOnly )
        return $result[0];
    else
        return count($result)>1 ? $result[0].'/'.$result[1] : $result[0];
}
