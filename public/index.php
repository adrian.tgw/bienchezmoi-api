<?php
declare(strict_types=1);


use Phalcon\Di\FactoryDefault;

error_reporting(E_ALL);

define('BASE_PATH', dirname(__DIR__));
define('APP_PATH', BASE_PATH . '/app');

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    // return only the headers and not the content
    // only allow CORS if we're doing a GET - i.e. no saving for now.
    
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']) &&
        ($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'] == 'GET' || $_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'] == 'POST')) {
      //header('Access-Control-Allow-Origin: *');
      
        $allowedOrigins = [
            "https://dashboard.appbienchezmoi.com",
            "https://appbienchezmoi.com"
        ];
        
        if (in_array($_SERVER["HTTP_ORIGIN"], $allowedOrigins)) {
            header("Access-Control-Allow-Origin: " . $_SERVER["HTTP_ORIGIN"]);
        }
        
        header('Access-Control-Allow-Headers: token');
    }
    exit;
}
//header('Access-Control-Allow-Origin: *');
//header('Access-Control-Allow-Methods: PUT, GET, POST, OPTIONS');
//header('Access-Control-Allow-Headers: token');

try {

    /**
     * The FactoryDefault Dependency Injector automatically registers
     * the services that provide a full stack framework.
     */
    $di = new FactoryDefault();
    
    include BASE_PATH . '/vendor/autoload.php';
    
    /**
     * Read services
     */
    include APP_PATH . '/config/services.php';
    
    /**
     * Get config service for use in inline setup below
     */
    $config = $di->getConfig();
    
    /**
     * Include Autoloader
     */
    include APP_PATH . '/config/loader.php';
    
    $app = new Phalcon\Mvc\Micro();
    
    /**
     * Handle routes
     */
    include APP_PATH . '/config/router.php';

    // Initialise the mongo DB connection.
    $di->setShared('mongo', function () {
        /** @var \Phalcon\DiInterface $this */
        $config = $this->getConfig();

        if (!$config->database->username || !$config->database->password) {
            $dsn = 'mongodb://' . $config->database->host;
        } else {
            $dsn = sprintf(
                'mongodb://%s:%s@%s',
                $config->database->username,
                $config->database->password,
                $config->database->host
            );
        }
        //$mongo = new \MongoDB\Driver\Manager($dsn . '/' . $config->database->dbname);
        //return $mongo;
        $mongo = new Phalcon\Db\Adapter\MongoDB\Client($dsn);
        return $mongo->selectDatabase($config->database->dbname);
    });
    
    $di->set('collectionManager', function(){
        return new \Phalcon\Mvc\Collection\Manager();
    }, true);
    
    $app->setDI($di);
    
    $app->notFound(
        function () use ($app) {
            $app->response->setStatusCode(404, "Not Found");

            $app->response->sendHeaders();
        }
    );

    /**
     * Handle the request
     */
    $application = new \Phalcon\Mvc\Application($di);
    //$application->getRouter()->setUriSource();

    echo str_replace(["\n","\r","\t"], '', $application->handle()->getContent());    
} catch (\Exception $e) {
    echo $e->getMessage() . '<br>';
    echo '<pre>' . $e->getTraceAsString() . '</pre>';
}
